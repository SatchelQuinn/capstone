// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "RoomBase.generated.h"

UCLASS()
class CAPSTONE_API ARoomBase : public AActor
{
	GENERATED_BODY()

		//Root component
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Generation", meta = (AllowPrivateAccess = "true"))
		class USceneComponent* SceneComponent;

	//Static mesh of the room 
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Generation", meta = (AllowPrivateAccess = "true"))
		class UStaticMeshComponent* RoomMesh;
	
	//Static mesh of the room 
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Generation", meta = (AllowPrivateAccess = "true"))
		class UGeneratorBounds* GeneratorBounds;
public:	
	// Sets default values for this actor's properties
	ARoomBase();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};
