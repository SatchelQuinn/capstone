// Fill out your copyright notice in the Description page of Project Settings.

#include "Hitbox.h"
#include "AttackBase.h"
#include "BaseCharacter.h"
#include "Engine/World.h"
#include "DamageInterface.h"
#include "GameFramework/PawnMovementComponent.h"
#include "Kismet/KismetMathLibrary.h"


// Sets default values for this component's properties
UHitbox::UHitbox()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	//Delegate on component overlap events
	OnComponentBeginOverlap.AddDynamic(this, &UHitbox::OnHitboxBeginOverlap);
	OnComponentEndOverlap.AddDynamic(this, &UHitbox::OnHitboxEndOverlap);
	// ...

	//Initalize
	index = 0;
	BaseDamage = 1.f;
	FinalDamage = BaseDamage;
	bHitOnlyOnce = false;
	bHitUniqueActors = false;
	bHasKnockback = false;
	KnockbackMultiplier;
	bPierceBarriers = false;
	bSelfDamage = false;
	bFriendlyFire = false;
	FriendlyDamageType = DT_Heal;
	FriendlyFireMultiplier = 1.0f;
	bAffectsWorld = true;
	bIsSweetspot = false;
	OwningCharacter = nullptr;
	OwningActor = nullptr;

}


// Called when the game starts
void UHitbox::BeginPlay()
{
	Super::BeginPlay();

	// ...
	
}

ABaseCharacter * UHitbox::GetHitboxOwningCharacter()
{
	return OwningCharacter;
	/*
	ABaseCharacter* MyOwner = Cast<ABaseCharacter>(GetOwner());
	if (MyOwner == nullptr)
		ABaseCharacter* MyOwner = Cast<ABaseCharacter>(GetOwner()->GetOwner());
	return MyOwner;
	*/
}


// Called every frame
void UHitbox::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

void UHitbox::OnHitboxBeginOverlap(UPrimitiveComponent * OverlappedComp, AActor * OtherActor, UPrimitiveComponent * OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult & SweepResult)
{
	//UE_LOG(LogTemp, Warning, TEXT("Overlap event"));
	
	if (OtherActor != GetOwner() && OtherActor != nullptr)
	{
		//Init Hit vectors
		FVector HitLocation = OtherActor->GetActorLocation();
		FVector HitNormal = FVector(0.f, 0.f, 1.f);
		if (OwningActor)
		{
			HitNormal = OtherActor->GetActorLocation() - OwningActor->GetActorLocation();
		}
		HitNormal.Normalize();
		
		//trace for target
		FHitResult Hit;
		FVector StartLocation = GetComponentLocation();
		//FVector EndLocation = OtherActor->GetActorLocation();
		FVector EndLocation = OtherComp->GetComponentLocation();
		FCollisionQueryParams CollisionParameters;
		CollisionParameters.AddIgnoredActor(GetOwner());
		CollisionParameters.bTraceComplex = true;
		GetWorld()->LineTraceSingleByChannel(Hit, StartLocation, EndLocation, ECollisionChannel::ECC_Pawn, CollisionParameters);
		//DrawDebugLine(GetWorld(), StartLocation, EndLocation, FColor::Green, true, -1, 0, 1.f);
		if (Hit.bBlockingHit)
		{
			HitLocation = Hit.ImpactPoint;
			HitNormal = Hit.ImpactNormal;
			UE_LOG(LogTemp, Warning, TEXT("attack Trace hit"));
		}
		ProcessOverlap(OtherActor, OtherComp, HitLocation, HitNormal);
	}
	
}

void UHitbox::OnHitboxEndOverlap(UPrimitiveComponent * OverlappedComp, AActor * OtherActor, UPrimitiveComponent * OtherComp, int32 OtherBodyIndex)
{
}

void UHitbox::ProcessOverlap(AActor* OtherActor, UPrimitiveComponent * OtherComp, FVector HitLoc, FVector HitNormal)
{
	//Make sure we aren't overlapping ourselves and we aren't testing against a blocked actor
	if (OtherActor != GetOwner() && OtherActor && (!BlockedActors.Contains(OtherActor) || bPierceBarriers) && (!bHitUniqueActors || !HitActors.Contains(OtherActor)))
	{
		//UE_LOG(LogTemp, Warning, TEXT("Overlap event2"));
		ABaseCharacter* TargetCharacter = Cast<ABaseCharacter>(OtherActor);
		AAttackBase* TargetAttackActor = Cast<AAttackBase>(OtherActor);
		bool bAttackSuccess = false;
		//See if we are hitting a barrier
		TSet<AActor*> OverlappingAttacks;
		GetOverlappingActors(OverlappingAttacks, AAttackBase::StaticClass());
		TArray<AAttackBase*> OverlappingBarriers;
		bool TargetBehindBarrier = false;
		for (AActor* at : OverlappingAttacks)
		{
			//Are we currently testing against this attack? If so, ignore it.
			if (OtherActor != at)
			{
				//cast to an attack object and see if it blocks
				AAttackBase* castat = Cast<AAttackBase>(at);
				if (castat)
				{
					//is this a barrier?
					if (castat->bBlocksIncomingDamage)
					{
						OverlappingBarriers.AddUnique(castat);
					}
				}
			}
		}

		
		
		//target is a character
		if (TargetCharacter)
		{
			//UE_LOG(LogTemp, Warning, TEXT("Overlap event: target is a character"));
			//UE_LOG(LogTemp, Warning, TEXT("bSelfDamage (%i)\nTargetCharacter != OwningCharacter = (%i)\n!TargetCharacter = (%i)\nOwningActor = (%i)"), bSelfDamage, TargetCharacter != OwningCharacter, !TargetBehindBarrier, OwningActor);
			if ((bSelfDamage || TargetCharacter != OwningCharacter) && !TargetBehindBarrier && OwningActor)
			{
				//Teams match and friendly fire is on
				if (bFriendlyFire && (OwningActor->Team == TargetCharacter->Team || OwningActor->Team == CT_NoTeam))
				{
					bAttackSuccess = true;
					TargetCharacter->OnCharacterDamage(FinalDamage * FriendlyFireMultiplier, index, FriendlyDamageType, GetHitboxOwningCharacter(), OwningActor, HitLoc, HitNormal);
					UE_LOG(LogTemp, Warning, TEXT("Attacking friendly"));
					if (bHasKnockback && FriendlyDamageType != DT_Heal)
					{
						ProcessKnockback(OtherComp, OtherActor);
					}
				}
				//teams don't match. Inflict damage.
				else if (OwningActor->Team != TargetCharacter->Team || OwningActor->Team == CT_NoTeam)
				{
					TargetBehindBarrier = TraceForBarriers(OverlappingBarriers, OtherActor);
					if (!BlockedActors.Contains(OtherActor))
					{
						bAttackSuccess = true;
						TargetCharacter->OnCharacterDamage(FinalDamage, index, DamageType, GetHitboxOwningCharacter(), OwningActor, HitLoc, HitNormal);
						UE_LOG(LogTemp, Warning, TEXT("Attacking foe"));
						if (bHasKnockback)
						{
							ProcessKnockback(OtherComp, OtherActor);
						}
					}
				}
				/*else
				{
					UE_LOG(LogTemp, Warning, TEXT("i dunno wut the fuk i hit"));
				}*/
			}
			/*else
			{
				UE_LOG(LogTemp, Warning, TEXT("ummmmmm wut?"));
			}*/
		}
		//target is an attack actor 
		else if (TargetAttackActor && OwningActor)
		{
			//Make sure damage is applied to attacks that care
			if (!TargetAttackActor->bIgnoreAllDamage)
			{
				if (OwningActor->Team != TargetAttackActor->Team || OwningActor->Team == CT_NoTeam)
				{
					TargetBehindBarrier = TraceForBarriers(OverlappingBarriers, OtherActor);
					if (!BlockedActors.Contains(OtherActor))
					{
						bAttackSuccess = true;
						TargetAttackActor->OnAttackActorDamage(FinalDamage, index, DamageType, GetHitboxOwningCharacter(), OwningActor, HitLoc, HitNormal);
						UE_LOG(LogTemp, Warning, TEXT("Attack clashed with attack"));
					}
				}
			}
		}
		//target is a damageable object
		else if (bAffectsWorld && OtherActor->GetClass()->ImplementsInterface(UDamageInterface::StaticClass()))
		{
			TargetBehindBarrier = TraceForBarriers(OverlappingBarriers, OtherActor);
			IDamageInterface::Execute_OnDamage(OtherActor, FinalDamage, index, DamageType, GetHitboxOwningCharacter(), OwningActor, HitLoc, HitNormal);
			bAttackSuccess = true;
			if (bHasKnockback)
			{
				ProcessKnockback(OtherComp, OtherActor);
			}
		}
		//Was the damage successful?
		if (bAttackSuccess)
		{
			HitActors.Add(OtherActor);
			if (OwningActor != nullptr)
				if (OwningActor->bDestroyOnHit)
				{
					OwningActor->DestroyAttack();
				}
				else
				{
					OwningActor->HitActors.AddUnique(OtherActor);
				}
			if (bHitOnlyOnce)
			{
				//disable collision of the component 
				SetCollisionEnabled(ECollisionEnabled::NoCollision);
			}

		}
	}
}

void UHitbox::ProcessKnockback(UPrimitiveComponent * OtherComp, AActor* OtherActor)
{
	ABaseCharacter* TargetCharacter = Cast<ABaseCharacter>(OtherActor);
	//add knockback to hit actor
	//Target is character
	if (TargetCharacter != nullptr)
	{
		FVector FinalKnockback;
		FRotator Rotation = UKismetMathLibrary::FindLookAtRotation(GetComponentLocation(), OtherActor->GetActorLocation());
		FinalKnockback = UKismetMathLibrary::GreaterGreater_VectorRotator(KnockbackMultiplier, Rotation);
		FinalKnockback.Z = KnockbackMultiplier.Z;
		if (TargetCharacter->Size == CS_ExtraLarge)
		{
			FinalKnockback * .1f;
		}
		else if (TargetCharacter->Size == CS_Large)
		{
			FinalKnockback * .5f;
		}
		//target is ragdoll
		if (TargetCharacter->GetMesh()->IsAnySimulatingPhysics())
		{
			TargetCharacter->GetMesh()->AddImpulse(FinalKnockback);
			return;
		}
		//Target is alive
		else
		{
			//check if target can receive knockback
			if (!(TargetCharacter->Vulnerability == DV_Superarmor || TargetCharacter->Vulnerability == DV_Invincible) && !TargetCharacter->GetMovementComponent()->IsFalling())
			{
				TargetCharacter->LaunchCharacter(FinalKnockback, false, true);
				if (FinalKnockback.Size() > 100.f)
				{
					TargetCharacter->InterruptChanneling();
				}
			}
			return;
		}
	}
	if (OtherComp->IsAnySimulatingPhysics())
	{
		FVector FinalKnockback;
		FinalKnockback = OtherComp->GetComponentLocation() - GetComponentLocation();
		FinalKnockback.Normalize();
		FinalKnockback *= KnockbackMultiplier;
		OtherComp->AddImpulse(FinalKnockback);
		return;
	}
}

bool UHitbox::TraceForBarriers(TArray<AAttackBase*> OverlappingBarriers, AActor* TargetActor)
{
	//iterate through all enemy barriers
	for (AAttackBase* eb : OverlappingBarriers)
	{
		if (OwningActor && eb != TargetActor)
		{
			//This is an enemy barrier
			if (eb->Team == CT_NoTeam || eb->Team != OwningActor->Team)
			{
				//line trace for barriers
				//FHitResult Hit;
				TArray<FHitResult> Hits;
				FVector StartLocation = GetComponentLocation();
				if (OwningActor && OwningCharacter)
				{
					//If this is a melee move, test against the character's location, not the center of the hitbox
					if (OwningActor->bIsMelee)
					{
						//StartLocation = OwningCharacter->GetActorLocation() + OwningActor->GetActorForwardVector()*-5.f;
					}
				}
				FVector EndLocation = TargetActor->GetActorLocation();
				FCollisionQueryParams CollisionParameters;
				CollisionParameters.AddIgnoredActor(GetOwner());
				if(OwningCharacter)
					CollisionParameters.AddIgnoredActor(OwningCharacter);
				//DrawDebugSphere(GetWorld(), EndLocation, 30.f, 5, FColor::White, false, 10.f, 0, 3.f);
				GetWorld()->LineTraceMultiByChannel(Hits, StartLocation, EndLocation, ECollisionChannel::ECC_GameTraceChannel2, CollisionParameters);
				//GetWorld()->LineTraceSingleByChannel(Hit, StartLocation, EndLocation, ECollisionChannel::ECC_GameTraceChannel2, CollisionParameters);
				//DrawDebugLine(GetWorld(), StartLocation, EndLocation, FColor::Green, true, -1, 0, 1.f);
				UE_LOG(LogTemp, Warning, TEXT("Target Character is %s"), *TargetActor->GetName());
				for (FHitResult h : Hits)
				{
					if (h.bBlockingHit)
					{
						//Target is behind the barrier
						//if (!BlockedActors.Contains(TargetActor))
						
						//Cast<ABaseCharacter>(TargetActor)
						UE_LOG(LogTemp, Warning, TEXT("traced to barrier success"));
						BlockedActors.AddUnique(TargetActor);

						
					}
				}
				return true;
			}
		}
	}
	return false;
}