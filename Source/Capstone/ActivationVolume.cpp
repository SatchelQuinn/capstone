// Fill out your copyright notice in the Description page of Project Settings.

#include "ActivationVolume.h"
#include "AISpawner.h"

// Sets default values for this component's properties
UActivationVolume::UActivationVolume()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = false;

	//Delegate on component overlap events
	OnComponentBeginOverlap.AddDynamic(this, &UActivationVolume::OnVolumeBeginOverlap);
	OnComponentEndOverlap.AddDynamic(this, &UActivationVolume::OnVolumeEndOverlap);
	// ...
}


// Called when the game starts
void UActivationVolume::BeginPlay()
{
	Super::BeginPlay();

	// ...
	
}


// Called every frame
void UActivationVolume::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

void UActivationVolume::OnVolumeBeginOverlap(UPrimitiveComponent * OverlappedComp, AActor * OtherActor, UPrimitiveComponent * OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult & SweepResult)
{
	UAISpawner* SpawnerRef = Cast<UAISpawner>(OtherComp);
	if (SpawnerRef)
	{
		if(!SpawnerRef->bDisableActivationVolumeSpawning)
			SpawnerRef->ActivateSpawner();
	}
}

void UActivationVolume::OnVolumeEndOverlap(UPrimitiveComponent * OverlappedComp, AActor * OtherActor, UPrimitiveComponent * OtherComp, int32 OtherBodyIndex)
{
	UAISpawner* SpawnerRef = Cast<UAISpawner>(OtherComp);
	if (SpawnerRef)
	{
		if (!SpawnerRef->bDisableActivationVolumeSpawning)
			SpawnerRef->DeactivateSpawner();
	}
}
