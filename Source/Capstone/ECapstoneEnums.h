// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "UObject/Class.h"
#include "CoreMinimal.h"

#define TEST_BIT(Bitmask, Bit) (((Bitmask) & (1 << static_cast<uint32>(Bit))) > 0)
#define SET_BIT(Bitmask, Bit) (Bitmask |= 1 << static_cast<uint32>(Bit))
#define CLEAR_BIT(Bitmask, Bit) (Bitmask &= ~(1 << static_cast<uint32>(Bit)))

/**
 * 
 
class CAPSTONE_API CapstoneEnums
{
public:
	CapstoneEnums();
	~CapstoneEnums();
};
*/
UENUM(BlueprintType)
enum EDamageType
{
	DT_Normal UMETA(DisplayName = "Normal"),
	DT_Fire UMETA(DisplayName = "Fire"),
	DT_Heal UMETA(DisplayName = "Healing"),
	DT_Poison UMETA(DisplayName= "Poison"),
	DT_Ice UMETA(DisplayName = "Ice"),
	DT_Electric UMETA(DisplayName = "Electric"),
	DT_None UMETA(DisplayName = "None")
};

UENUM(BlueprintType)
enum EAbilityType
{
	AT_Offensive UMETA(DisplayName = "Offensive"),
	AT_Utility UMETA(DisplayName = "Utility"),
	AT_Defensive UMETA(DisplayName = "Defensive"),
	AT_Melee UMETA(DisplayName = "Melee"),
	AT_Other UMETA(DisplayName = "Other")


};

UENUM(BlueprintType)
enum ECharacterTeam
{
	CT_Player UMETA(DisplayName = "Player"),
	CT_Enemy UMETA(DisplayName = "Enemy"),
	CT_Other UMETA(DisplayName = "Other"),
	CT_NoTeam UMETA(DisplayName = "No Team")
};

UENUM(BlueprintType)
enum EObjectiveType
{
	OT_Maze UMETA(DisplayName = "Maze"),
	CT_Timed UMETA(DisplayName = "Timed"),
	CT_Arena UMETA(DisplayName = "Arena"),
	CT_Payload UMETA(DisplayName = "Payload")
};

UENUM(BlueprintType)
enum ECursorType
{
	CT_Sky UMETA(DisplayName = "Sky"),
	CT_Target UMETA(DisplayName = "Target"),
	CT_Self UMETA(DisplayName = "Self"),
	CT_Direction UMETA(DisplayName = "Direction")
};

UENUM(BlueprintType)
enum EAnimationType
{
	AT_Self UMETA(DisplayName = "Self"),
	AT_Bolt UMETA(DisplayName = "Bolt"),
	AT_Target UMETA(DisplayName = "Target"),
	AT_Defense UMETA(DisplayName = "Defense"),
	AT_Swing UMETA(DisplayName = "Swing"),
	AT_Drain UMETA(DisplayName = "Drain"),
	AT_Beam UMETA(DisplayName = "Beam"),
	AT_BoltSmall UMETA(DisplayName = "Bolt Small"),
	AT_None UMETA(DisplayName = "None"),
	AT_None2 UMETA(DisplayName = "None2")
};

UENUM(BlueprintType)
enum ECharacterSize
{
	CS_Small UMETA(DisplayName = "Small"),
	CS_Medium UMETA(DisplayName = "Medium"),
	CS_Large UMETA(DisplayName = "Large"),
	CS_ExtraLarge UMETA(DisplayName = "Extra Large")
};

UENUM(BlueprintType)
enum EDamageVulnerability
{
	DV_Normal UMETA(DisplayName = "Normal"),
	DV_Invulnerable UMETA(DisplayName = "Invulnerable"),
	DV_Superarmor UMETA(DisplayName = "Super Armor"),
	DV_Invincible UMETA(DisplayName = "Invincible")
};


UENUM(BlueprintType, Meta = (Bitflags))
enum class EPropType: uint8
{
	PT_Pillar UMETA(DisplayName = "Pillar"),
	PT_Destructible UMETA(DisplayName = "Architecture"),
	PT_SmallProp UMETA(DisplayName = "Small Prop"),
	PT_Item UMETA(DisplayName = "Item"),
	PT_MiniArena UMETA(DisplayName = "MiniArena"),
	MAX_NUM_OF_PROPS
};
//ENUM_CLASS_FLAGS(EPropType)

//UENUM(BlueprintType)
enum EnumStatType
{
	ST_Precision, //UMETA(DisplayName = "Precision"),
	ST_Aggression, //UMETA(DisplayName = "Aggression"),
	ST_Resilience, //UMETA(DisplayName = "Resilience"),
	ST_Creativity, //UMETA(DisplayName = "Creativity"),
	ST_Resourcefulness, //UMETA(DisplayName = "Resourcefulness"),
	ST_Kindess
};//UMETA(DisplayName = "Kindess")

