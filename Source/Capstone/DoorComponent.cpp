// Fill out your copyright notice in the Description page of Project Settings.

#include "DoorComponent.h"
#include "GenericPlatformMath.h"

// Sets default values for this component's properties
UDoorComponent::UDoorComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = false;

	// ...
}


// Called when the game starts
void UDoorComponent::BeginPlay()
{
	Super::BeginPlay();

	// ...
	
}


// Called every frame
void UDoorComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

bool UDoorComponent::IsDoorCompatible(UDoorComponent * Door, TEnumAsByte<ERoomType> RoomType)
{
	int32 roommask = RoomType;
	roommask = pow(2,roommask);
	if (Door)
	{
		if (roommask & Door->RoomTypeFlags)
		{
			return true;
		}
	}

	return false;
}

