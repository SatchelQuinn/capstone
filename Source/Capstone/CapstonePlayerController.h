// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "CapstonePlayerController.generated.h"

/**
 * 
 */
UCLASS()
class CAPSTONE_API ACapstonePlayerController : public APlayerController
{
	GENERATED_BODY()

public:
	//Called when the special attack button is pressed
	UFUNCTION(BlueprintCallable, Category = "Controls")
		void OnAbilityButtonPressed();

	//Called when the special attack button is pressed
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Gameplay")
		void SpawnPlayerPawn();
		void SpawnPlayerPawn_Implementation();
};
