// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/BillboardComponent.h"
#include "ECapstoneEnums.h"
#include "Components/StaticMeshComponent.h"
#include "MiniArena.h"
#include "PropActor.h"
#include "PropCpp.generated.h"


UCLASS(ClassGroup=(Custom), Blueprintable) //, meta=(BlueprintSpawnableComponent) )
class CAPSTONE_API UPropCpp : public UBillboardComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UPropCpp();

	//Temporary Mesh pointer
	//UPROPERTY(BlueprintReadWrite, EditAnywhere, category = "Prop")
	//	UStaticMesh* Mesh;

	//Bitmask of prop types that are allowed to spawn
	//UPROPERTY(BlueprintReadWrite, EditAnywhere, category = "Prop", Meta = (Bitmask, BitmaskEnum = "EPropType"))
	//	int32 PropTypes;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, category = "Prop")
		TMap<EPropType, bool> PropTypeMap;

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;


	//Force this component to spawn a prop
	UFUNCTION(BlueprintCallable, category = "Prop")
		void SpawnProp();

	// Spawns an actor
	UFUNCTION(BlueprintCallable, category = "Prop")
		void SpawnActor(TSubclassOf<AActor> actor);

	// Spawns a mini arena and sets important varibles
	UFUNCTION(BlueprintCallable, category = "Prop")
		void SpawnMiniArena(TSubclassOf<AMiniArena> arena);

	// Spawns a component 
	UFUNCTION(BlueprintCallable, category = "Prop")
		void SpawnComponent(UStaticMesh *_mesh);

	// Array that hold all collectable items
	UPROPERTY(BlueprintReadWrite, EditAnywhere, category = "Prop")
		TArray<TSubclassOf<AActor>> Items;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, category = "Prop")
		TArray<TSubclassOf<APropActor>> Pillars;

	// NOTE: will change to a different type once implemented
	UPROPERTY(BlueprintReadWrite, EditAnywhere, category = "Prop")
		TArray<TSubclassOf<AActor>> Destructables;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, category = "Prop")
		TArray<TSubclassOf<APropActor>> SmallProps;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, category = "Prop")
		TArray<TSubclassOf<AMiniArena>> MiniArenas;
};
