// Fill out your copyright notice in the Description page of Project Settings.

#include "GrassGenerator.h"
#include "Components/InstancedStaticMeshComponent.h"
#include "GameFramework/Actor.h"

// Sets default values for this component's properties
UGrassGenerator::UGrassGenerator()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = false;

	// ...
}


// Called when the game starts
void UGrassGenerator::BeginPlay()
{
	Super::BeginPlay();

	// ...
	
}


// Called every frame
void UGrassGenerator::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

void UGrassGenerator::SpawnGrass()
{
	TArray<UStaticMesh*> MeshArray;
	MeshMap.GetKeys(MeshArray);
	for (UStaticMesh* mesh : MeshArray)
	{
		if (mesh)
		{
			AActor* MyOwner = GetOwner();
			UInstancedStaticMeshComponent *ISMComp = NewObject<UInstancedStaticMeshComponent>(MyOwner);
			ISMComp->RegisterComponent();
			ISMComp->SetStaticMesh(mesh);
			ISMComp->SetFlags(RF_Transactional);
			MyOwner->AddInstanceComponent(ISMComp);
			ISMComp->SetCollisionEnabled(ECollisionEnabled::NoCollision);

			for (int32 i = 0; i < MeshMap[mesh]; i++)
			{
				FTransform randomtrans = GetComponentTransform();
				FVector SpawnExtent = GetScaledBoxExtent();
				float randX = FMath::RandRange(-SpawnExtent.X, SpawnExtent.X);
				float randY = FMath::RandRange(-SpawnExtent.Y, SpawnExtent.Y);
				FVector FinalLoc;
				FinalLoc = randomtrans.TransformPosition(FVector(randX,randY,0.f));

				FVector randLoc = FVector(FinalLoc.X, FinalLoc.Y, GetComponentLocation().Z);
				randomtrans.SetLocation(randLoc);
				
				FQuat FinalRotation = randomtrans.GetRotation();
				if (bUseRandomRotation)
				{
					FRotator rot = GetComponentRotation();
					float randYaw = FMath::RandRange(0, 360);
					rot.Yaw = randYaw;
					FinalRotation = rot.Quaternion();
				}
				randomtrans.SetRotation(FinalRotation);
				float FinalScale = FMath::RandRange(RandomScale.X, RandomScale.Y);
				randomtrans.SetScale3D(FVector(FinalScale, FinalScale, FinalScale));
				ISMComp->AddInstance(randomtrans);
			}
		}
	}
}

