// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "DamageInterface.h"
#include "LevelUpStructs.h"
#include "TemporaryStats.h"
#include "BaseCharacter.generated.h"

#define GETENUMSTRING(etype, evalue) ( (FindObject<UEnum>(ANY_PACKAGE, TEXT(etype), true) != nullptr) ? FindObject<UEnum>(ANY_PACKAGE, TEXT(etype), true)->GetEnumName((int32)evalue) : FString("Invalid - are you sure enum uses UENUM() macro?") )

UCLASS() 
class CAPSTONE_API ABaseCharacter : public ACharacter , public IDamageInterface
{
	GENERATED_BODY()

	/** Status Particle effect. Used for focusing and channeling*/
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Effects, meta = (AllowPrivateAccess = "true"))
	class UParticleSystemComponent* StatusParticle;

public:
	// Sets default values for this character's properties
	ABaseCharacter();

	//The team this character belongs to. Used for friendly fire calculations.
	UPROPERTY(BlueprintReadWrite, EditAnywhere, category = "Attributes")
		TEnumAsByte<ECharacterTeam> Team;
	
	//Character's current Level. Starts at 1 and increases on each LevelUp call
	UPROPERTY(BlueprintReadWrite, EditAnywhere, category = "Stats")
		int32 Level;

	//Character's current experience value. Resets to zero after a level up.
	UPROPERTY(BlueprintReadWrite, EditAnywhere, category = "Stats")
		int32 EXP;

	//The amount of exp character needs before levelup is called
	UPROPERTY(BlueprintReadWrite, EditAnywhere, category = "Stats")
		int32 EXPNextLevel;

	//The names of the character's abilities. To be used in looking up the ability data table.
	UPROPERTY(BlueprintReadWrite, EditAnywhere, category = "Ability")
		TArray<FName> AbilityNames;

	//The data table to be used for ability lookups 
	UPROPERTY(BlueprintReadWrite, EditAnywhere, category = "Ability")
		UDataTable* AbilityTable;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, category = "Ability")
		TArray<FAbilityData> CurrentAbilities;

	//The data table to be used for ability upgrades 
	UPROPERTY(BlueprintReadWrite, EditAnywhere, category = "Ability")
		UDataTable* AbilityUpgradesTable;

	//Character's current health value. Cannot exceed MaxHealth
	UPROPERTY(BlueprintReadWrite, EditAnywhere, category = "Stats")
		float Health;
	
	//The maximum amount of health this character can have. Only changed by the LevelUp function
	UPROPERTY(BlueprintReadWrite, EditAnywhere, category = "Stats")
		float MaxHealth;

	//Total magic points this player can have. Cannot exceed MaxMagic
	UPROPERTY(BlueprintReadWrite, EditAnywhere, category = "Stats")
		float Magic;

	//The maximum amount of magic this character can have. Only changed by the LevelUp function
	UPROPERTY(BlueprintReadWrite, EditAnywhere, category = "Stats")
		float MaxMagic;

	//The rate at which magic regenerates naturally per second
	UPROPERTY(BlueprintReadWrite, EditAnywhere, category = "Stats")
		float MagicRegenRate;

	//The multiplier used when the player is focusing magic regeneration
	UPROPERTY(BlueprintReadWrite, EditAnywhere, category = "Stats|Extras")
		float MagicRegenMultiplier;

	//This character's melee attack stat. Divided by 100 to get the hitbox attack multiplier
	UPROPERTY(BlueprintReadWrite, EditAnywhere, category = "Stats")
		float BaseMeleeAttack;

	//The temporary multiplier applied to our melee attack stat. Reverts to 1 after the buff wears off 
	UPROPERTY(BlueprintReadWrite, EditAnywhere, category = "Stats|Temp")
		float MeleeAttackMultiplier;

	//This character's magic attack stat. Divided by 100 to get the hitbox attack multiplier
	UPROPERTY(BlueprintReadWrite, EditAnywhere, category = "Stats")
		float BaseMagicAttack;

	//The temporary multiplier applied to our magic attack stat. Reverts to 1 after the buff wears off 
	UPROPERTY(BlueprintReadWrite, EditAnywhere, category = "Stats|Temp")
		float MagicAttackMultiplier;

	//Multiplier used to affect the damage/healing amount towards friendlies
	UPROPERTY(BlueprintReadWrite, EditAnywhere, category = "Stats|Extras")
		float FriendlyFireMultiplier;
	
	//Speed value used when not dashing. Referenced by the UpdateSpeed function
	UPROPERTY(BlueprintReadWrite, EditAnywhere, category = "Stats")
		float BaseSpeed;

	//Speed value used when dashing. Referenced by the UpdateSpeed function
	UPROPERTY(BlueprintReadWrite, EditAnywhere, category = "Stats")
		float DashSpeed;

	//Maximum Speed stat this character can have
	UPROPERTY(BlueprintReadWrite, EditAnywhere, category = "Stats|Extras")
		float MaxSpeed;

	//Is the cursor active? Is false on player move
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Condition")
		bool bCursorActive;

	//Is the cursor following the character's rotation? true for only some cursor types
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Condition")
		bool bCursorFollowsRotation;

	//Speed The cursor can move per second
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Stats|Extras")
		float CursorSpeed;

	//How far the cursor can be from the character. Only takes X and Y into account.
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Stats|Extras")
		float CursorRange;

	//Set true when the character is focusing magic regeneration
	UPROPERTY(BlueprintReadWrite, EditAnywhere, category = "Condition")
		bool bIsFocusing;

	//Set true when the character is dashing. Set false when the player stops moving or starts falling. 
	UPROPERTY(BlueprintReadWrite, EditAnywhere, category = "Attributes")
		bool bCanDash;

	//Time the player must wait before a dash is initiated
	UPROPERTY(BlueprintReadWrite, EditAnywhere, meta = (category = "Stats|Extras", EditCondition = "bCanDash"))
		float DashDelay;

	//Set true when the character is dashing. Set false when the player stops moving or starts falling.
	UPROPERTY(BlueprintReadWrite, EditAnywhere, category = "Condition")
		bool bIsDashing;

	//Is true when the character is dead and not able to be revived 
	UPROPERTY(BlueprintReadWrite, EditAnywhere, category = "Condition")
		bool bIsDead;

	//Is set true when this character loses all health and can revive
	UPROPERTY(BlueprintReadWrite, EditAnywhere, category = "Condition")
		bool bIsIncapacitated;

	//Is set true when this character starts an attack that restricts movement
	UPROPERTY(BlueprintReadOnly, category = "Condition")
		bool bIsChanneling;

	//Is set true when this character starts an attack that restricts movement
	UPROPERTY(BlueprintReadOnly, category = "Condition")
		bool bIsGrabbed;

	//Is set true when this character is standing on the ground, or when a ray trace hits ground beneath them.
	UPROPERTY(BlueprintReadWrite, EditAnywhere, category = "Condition")
		bool bIsAboveGround;

	//Map of this character's weaknesses. Is multiplied by the base damage to get the final damage output.
	UPROPERTY(BlueprintReadWrite, EditAnywhere, category = "Attributes")
		TMap<TEnumAsByte<EDamageType>, float> WeaknessMap;

	//The current value of the custom depth stencil. Based on team and Player Index.
	UPROPERTY(BlueprintReadOnly, meta = (category = "Attributes"))
		int32 Stencil;

	//The size of this character. Used for ability capture thresholds.
	UPROPERTY(BlueprintReadWrite, EditAnywhere, category = "Attributes")
		TEnumAsByte<ECharacterSize> Size;

	//The vulernablity of this character. Used when calculating damage and knockback.
	UPROPERTY(BlueprintReadOnly, category = "Attributes")
		TEnumAsByte<EDamageVulnerability> Vulnerability;

	//Is this character spawned by another character as a minion?
	UPROPERTY(BlueprintReadWrite, EditAnywhere, category = "Attributes")
		bool bIsMinion;

	//Tells the world's camera to keep focus on this character. 
	UPROPERTY(BlueprintReadWrite, EditAnywhere, category = "Attributes")
		bool bCanBeCameraTarget;

	//Is the camera currently focused on this character?
	UPROPERTY(BlueprintReadWrite, EditAnywhere, category = "Attributes")
		bool bIsCameraTarget;

	//Set true when the character is dashing. Set false when the player stops moving or starts falling.
	UPROPERTY(BlueprintReadWrite, EditAnywhere, category = "Attributes")
		bool bCanLevelUp;

	//Is true if the player has the chance to hang on
	UPROPERTY(BlueprintReadWrite, EditAnywhere, category = "Attributes")
		bool bCanLastStand;

	//Time the player will be in Last Stand mode before returning to normal
	UPROPERTY(BlueprintReadWrite, EditAnywhere, meta = (category = "Stats|Extras", EditCondition = "bCanLastStand"))
		float LastStandTime;

	//Is true if the player is in Last Stand Mode. If they get hit in this mode, they will perish.
	UPROPERTY(BlueprintReadWrite, EditAnywhere, category = "Condition")
		bool bIsLastStanding;

	//Can This Character give EXP?
	UPROPERTY(BlueprintReadWrite, EditAnywhere, category = "Attributes")
		bool bGivesEXP;

	//Can This Character revive itself when downed?
	UPROPERTY(BlueprintReadWrite, EditAnywhere, category = "Attributes")
		bool bCanRevive;

	//Can This Character revive a downed ally?
	UPROPERTY(BlueprintReadWrite, EditAnywhere, category = "Attributes")
		bool bCanReviveAlly;

	//Attribute set true upon void out. set false after 1 second
	UPROPERTY(BlueprintReadOnly, category = "Attributes")
		bool bVoidOutCooldown;

	//Raw EXP value awarded to players on death
	UPROPERTY(BlueprintReadWrite, EditAnywhere, meta = (category = "Stats|Extras", EditCondition = "bGivesEXP"))
		float BaseEXPYield;

	//Array of characters that damaged this character. Used for experience giving.
	UPROPERTY(BlueprintReadWrite, EditAnywhere, meta = (category = "Attributes", EditCondition = "bGivesEXP"))
		TArray<ABaseCharacter*> DamagedBy;

	//Used when this character is being revived by an ally. When it fills to 1.0, call the revive function.
	UPROPERTY(BlueprintReadWrite, EditAnywhere, meta = (category = "Attributes", EditCondition = "bCanRevive"))
		float ReviveAmount;

	//The amount added to the reviveamount per second per character
	UPROPERTY(BlueprintReadWrite, EditAnywhere, meta = (category = "Stats|Extras", EditCondition = "bCanRevive"))
		float ReviveSpeed;

	//The last transform we known to be safe. Initalized to the current transform on construct
	UPROPERTY(BlueprintReadOnly, meta = (category = "Stats|Data"))
		TArray<FTransform> LastSafeTransform;

	//Attack we are currently channeling. Will be destroyed when channeling is interrupted.
	UPROPERTY(BlueprintReadOnly, meta = (category = "Stats|Data"))
		AAttackBase* ChanneledAttack;

	//The fraction of this character's health that will be subtracted upon falling into the abyss
	UPROPERTY(BlueprintReadWrite, EditAnywhere, meta = (category = "Stats|Extras", ClampMin = "0.0", ClampMax = "1.0", UIMin = "0.0", UIMax = "1.0"))
		float VoidOutPercent;

	//Contains variables for determining level ups / ability counters
	UPROPERTY(BlueprintReadWrite, EditAnywhere, meta = (category = "Stats|Data"))
	FTemporaryStats TemporaryStats;
	//Contains the original weights for easy replacements of the temp variables
	FTemporaryStats OriginalStats;
	//Contains variables that will persist between level ups
	UPROPERTY(BlueprintReadWrite, EditAnywhere, meta = (category = "Stats|Data"))
	FPersistentStats PersistentStats;

	//Dynamic Material of the character. Set on Construct.
	UPROPERTY(BlueprintReadWrite, meta = (category = "Params"))
		UMaterialInstanceDynamic* DyMat;

	//Main stat groups for each of the main 6 stats. Will be increased by the tracked stats, and then used to determine qualifiers.
	UPROPERTY(BlueprintReadWrite, EditAnywhere, category = "Stats|Behavior")
		int32 Precision;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, category = "Stats|Behavior")
		int32 Aggression;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, category = "Stats|Behavior")
		int32 Resilience;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, category = "Stats|Behavior")
		int32 Creativity;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, category = "Stats|Behavior")
		int32 Resourcefulness;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, category = "Stats|Behavior")
		int32 Kindness;
	
	TMap<EnumStatType, int> MainStatTypes;

	//Tracked stats
	//go in Levelup Struct


protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	//Counter used for the dash timer
	float DashTimerCounter = 0.f;

	//Timer handles
	FTimerHandle FDashTimer;
	FTimerHandle FSafeLocationTimer;
	FTimerHandle FVoidOutCooldownTimer;
	FTimerHandle FSweepForNearbyCharacters;
	FTimerHandle FCursorActive;

	//Damage Varibles
	FTimerHandle DamageTimer;
	float DamageLast = 0.f;
	int32 DamageHitboxIndex = 999;
	EDamageType DamageLastType;
	ABaseCharacter* LastAttacker;
	AAttackBase* LastAttackActor = nullptr;
	FVector HitLoc = FVector(0.f,0.f,0.f);
	FVector HitNorm = FVector(0.f, 0.f, 0.f);

	//Qualifiers you achieved
	TArray<EUpgradeQualifier> permanentQualifiersAchieved;

	bool CanShowUpgradeHint = true; 

	//Variables for determining level ups / ability counters

	//NOTE: weights not from doc currently set to 0.5f
	
public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	//Sets the character's mesh to be rendered in the correct custom depth mode
	void SetCharacterCustomDepth();

	//Converts speed stat scale to the character movement scale and updates the max walk speed
	UFUNCTION(BlueprintCallable, category = "Gameplay|Character")
	void UpdateSpeed();

	//Updates the focus state and the character's speed. Optional parameter to change the attached particle effect. Returns true on successful state change.
	UFUNCTION(BlueprintCallable, category = "Gameplay|Character")
		bool SetFocusingState(bool NewState, UParticleSystem* ParticleEffect = nullptr);

	//Updates the channeling state and the character's speed. Optional parameter to change the attached particle effect. Returns true on successful state change.
	UFUNCTION(BlueprintCallable, category = "Gameplay|Character")
		bool SetChannelingState(bool NewState, UParticleSystem* ParticleEffect = nullptr);

	//Updates the grab state and the character's speed. Optional parameter to change the attached particle effect. Returns true on successful state change.
	UFUNCTION(BlueprintCallable, category = "Gameplay|Character")
		bool SetGrabState(bool NewState, USceneComponent* GrabbingComponent, FName AttachSocketName, UParticleSystem* ParticleEffect = nullptr);

	//Increases the player's level and grants a new ability
	UFUNCTION(BlueprintCallable, category = "Gameplay|Character")
	void LevelUp();

	void UpdatePlayerVariablesOnTick();

	//Called by LevelUp() to determine the array of qualifiers
	TArray<EUpgradeQualifier> DetermineValidQualifiers();
	TArray<FAbilityUpgradeData *> QueryAbilityUpgradesDataTable();
	FAbilityUpgradeData *DetermineNewAbility(TArray<EUpgradeQualifier> qualifiers, TArray<FAbilityUpgradeData *> upgradeData);
	int32 ApplyNewUpgrade(FAbilityUpgradeData *chosenUpgrade);

	//Calculates the player's stat varibles from their behavior stats
	UFUNCTION(BlueprintCallable, category = "Gameplay|Character")
	void UpdatePlayerStats();
	void ApplyPermanentStats();

	//Blueprint Event that's triggered upon level up
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, category = "Gameplay|Character")
	void OnLevelUp(FName NewAbility, EAbilityType AbilityType);
	virtual void OnLevelUp_Implementation(FName NewAbility, EAbilityType AbilityType);

	FString GetUpgradeHint(TArray<EUpgradeQualifier> qualifiers, TArray<FAbilityUpgradeData *> upgradeData);
	void DetermineUpgradeHint();

	//Figure out the hint for your next upgrade
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, category = "Gameplay|Character")
	void OnUpgradeHint(FName NewHint);
	virtual void OnUpgradeHint_Implementation(FName NewHint);

	//Looks up the abilities in the data table and replaces the selected ability array
	UFUNCTION(BlueprintCallable, category = "Gameplay|Character")
	void LookUpAbilities(TArray<FName> NewNames);

	//Checks to see if the character has enough magic for the attack
	UFUNCTION(BlueprintCallable, category = "Gameplay|Character")
	bool CheckMagicRequirement(float MagicRequirement);

	//Checks to see if the character fullfils the correct state requirement to act
	UFUNCTION(BlueprintCallable, BlueprintPure, category = "Gameplay|Character")
	bool CanCharacterAct();

	//Subtracts Health from the character. Calls Death when the player dies.
	UFUNCTION(BlueprintCallable, category = "Gameplay|Damage")
	void DamageCharacter(float Damage);
	
	//Called when the character loses all their health, but can be revived 
	UFUNCTION(BlueprintCallable, category = "Gameplay|Character")
		void Incapacitate();
	
	//Blueprint function that is called when the character loses all their health, but can be revived 
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, category = "Gameplay|Character")
		void OnIncapacitate();
	virtual void OnIncapacitate_Implementation();

	//Called when the character dies.
	UFUNCTION(BlueprintCallable, category = "Gameplay|Character")
		void Death();

	//Blueprint function that is called when the character dies.  
	UFUNCTION (BlueprintCallable, BlueprintNativeEvent, category = "Gameplay|Character")
	void OnDeath();
	virtual void OnDeath_Implementation();

	//Adds magic to the character's magic pool. Clamps it to MaxMagic
	UFUNCTION(BlueprintCallable, category = "Gameplay|Character")
	void UpdateMagic(float DeltaMagic);

	//Function that is called when the player is ready to dash. Sets speed to the dash speed. 
	UFUNCTION (BlueprintCallable, BlueprintNativeEvent, category = "Gameplay|Character")
	void StartDash();
	void StartDash_Implementation();

	//Runs on a timer. Checks to see if the pawn is moving, not falling, holding the control stick more than half way, and not already dashing. If true, calls StartDash.
	UFUNCTION(BlueprintCallable, category = "Gameplay|Character")
	void CheckDash();

	//Resets varibles and changes speed back to the base. Implemented by Blueprint.
	UFUNCTION (BlueprintCallable, BlueprintNativeEvent, category = "Gameplay|Character")
	void EndDash();
	void EndDash_Implementation();

	//Sets the character's experience. Returns true on level up.
	UFUNCTION(BlueprintCallable, category = "Gameplay|Character")
	bool GetEXP(float DeltaEXP);

	//Gives out exp to all players who hurt this character
	UFUNCTION(BlueprintCallable, category = "Gameplay|Character")
	void GiveEXP();

	//Is this character an ally or ourselves?
	UFUNCTION(BlueprintCallable, BlueprintPure, category = "Gameplay|Character")
		static bool IsTargetAlly(ABaseCharacter* TargetCharacter, ABaseCharacter* OtherCharacter, bool &bIsSelf);

	//Is this character under this size threshold?
	UFUNCTION(BlueprintCallable, BlueprintPure, category = "Gameplay|Character")
	static bool IsCharacterSmallEnough(ABaseCharacter * TargetCharacter, TEnumAsByte<ECharacterSize> LargestSize);

	//Damage Interface Function. Sets damage based on type weakness.
	UFUNCTION(BlueprintCallable, Category = "Gameplay|Damage")
	void OnCharacterDamage(float Damage, int32 HitboxIndex, EDamageType Type, ABaseCharacter* DamageCauser, AAttackBase* AttackActor, FVector HitLocation, FVector HitNormal);

	//Called one frame after a hitbox overlaps this actor. Clear all the temporary damage varibles, subtracts health, adds the damage causer to the array and calls PostDamage
	UFUNCTION(BlueprintCallable, category = "Gameplay|Damage")
	void OnCharacterDamageInflict();

	//Blueprint event called after damage is inflicted on this character. 
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, category = "Gameplay|Damage")
	void PostDamage(float Damage, EDamageType DamageType, ABaseCharacter* DamageCauser, AAttackBase* AttackActor, FVector HitLocation, FVector HitNormal);
	void PostDamage_Implementation(float Damage, EDamageType DamageType, ABaseCharacter* DamageCauser, AAttackBase* AttackActor, FVector HitLocation, FVector HitNormal);
	

	//Function that will increment variables based on what kind of attackl was used
	UFUNCTION(BlueprintCallable, category = "Gameplay|Character")

	//Blueprint event that should be called after every attack has been performed
	void PostAttackInformationUpdate(AAttackBase *AttackActor);

	//Function that fires when a character takes damage from this character's attack
	UFUNCTION(BlueprintCallable, category = "Gameplay|Damage")
	void OnAttackSuccess(float Damage, EDamageType Type, ABaseCharacter* DamagedCharacter, AAttackBase* AttackActor, FVector HitLocation, FVector HitNormal);

	//Blueprint event called after another character takes damage from this character's attack
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, category = "Gameplay|Damage")
	void PostAttackSuccess(float Damage, EDamageType Type, ABaseCharacter* DamagedCharacter, AAttackBase* AttackActor, FVector HitLocation, FVector HitNormal, bool KilledTarget);
	void PostAttackSuccess_Implementation(float Damage, EDamageType Type, ABaseCharacter* DamagedCharacter, AAttackBase* AttackActor, FVector HitLocation, FVector HitNormal, bool KilledTarget);

	UFUNCTION(BlueprintCallable, category = "Gameplay|Damage")
		void OnHitDestructableMesh(int ResourcefulnessAmount);

	//Adds some amount to the revive amount. Calls the revive function when revive amount == 1
	UFUNCTION(BlueprintCallable, category = "Gameplay|Character")
		void ReviveTick(float DeltaSeconds);

	//Resets the revive amount and the player state in the parent function
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, category = "Gameplay|Character")
		void Revive(ABaseCharacter* AllyWhoRevived);
	void Revive_Implementation(ABaseCharacter* AllyWhoRevived);

	//Called when the character falls into the abyss. Subtracts health and sets the 
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, category = "Gameplay|Character")
		void OnVoidOut();
	void OnVoidOut_Implementation();

	//Searched the last safe transform array and traces the ground for a safe place to relocate the character
	UFUNCTION(BlueprintCallable, category = "Gameplay|Character")
		FTransform GetSafeTransform();

	//Destroys channeling attack and sets the channeling state to false 
	UFUNCTION(BlueprintCallable, category = "Gameplay|Character")
		void InterruptChanneling();

	//Sets the vulnerability state of this character. Option to change material params? Time option?
	UFUNCTION(BlueprintCallable, category = "Gameplay|Character")
		void SetVulnerabilityState(TEnumAsByte<EDamageVulnerability> NewState);

	//Returns the appropiate attack multiplier to be accessed at the time of attack spawn
	UFUNCTION(BlueprintCallable, category = "Gameplay|Character")
		float GetAttackMultiplier(bool bIsMelee = false);

	//Runs per tick. Regerates magic when not casting or dead. Regerates slower during Last Stand? 
	void RegenMagic(float DeltaTime);

	//Runs on a timer that saves the last "safe" location. To be used when repositioning the player after a fall.
	void SetSafeLocation();
	
	//Event called when the Void out cooldown is complete
	void VoidOutCooldown();

	void SweepForCharactersNearby();
	void CheckIfCursorActive();
};
