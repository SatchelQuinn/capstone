// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/GameInstance.h"
#include "PlayerData.h"
#include "Engine/SkeletalMesh.h"
#include "CapstoneGameInstance.generated.h"

/**
 * 
 */
UCLASS()
class CAPSTONE_API UCapstoneGameInstance : public UGameInstance
{
	GENERATED_BODY()

public:
		//An array of data to be preserved 
		UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Player")
		TArray<FPlayerData> PlayersData;

	//All color variations a male character could have
		UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Player")
			TArray<UMaterialInterface*> CharacterMatsM;

		//All color variations a female character could have
		UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Player")
			TArray<UMaterialInterface*> CharacterMatsF;

		//All color variations a player could have
		UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Player")
			TArray<UMaterialInterface*> CharacterMatsAll;

	//All model variations a player could have
		UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Player")
			TArray<USkeletalMesh*> CharacterModels;

	UFUNCTION(BlueprintCallable, Category = "Gameplay")
		void PackPlayerData(int32 PlayerIndex);

};
