// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/BoxComponent.h"
#include "GrassGenerator.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class CAPSTONE_API UGrassGenerator : public UBoxComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UGrassGenerator();

	UPROPERTY(BlueprintReadWrite, EditAnywhere, category = "Prop")
		TMap<UStaticMesh*, int32> MeshMap;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, category = "Prop")
		bool bUseRandomRotation;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, category = "Prop")
		FVector2D RandomScale = FVector2D(1.f,1.f);

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	UFUNCTION(BlueprintCallable, category = "Prop")
	void SpawnGrass();
};
