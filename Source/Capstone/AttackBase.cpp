// Fill out your copyright notice in the Description page of Project Settings.

#include "AttackBase.h"
#include "Hitbox.h"

// Sets default values
AAttackBase::AAttackBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	GetComponents<UHitbox>(Hitboxes);
	WeaknessMap.Add(DT_Heal, -1.f);


	//Initialize

	//Public Vars
	AbilityType = AT_Offensive;
	Team = CT_NoTeam;
	Health = 0.f;
	AttackMultiplier = 1.f;
	bIsMelee = false;
	bDestroyOnHit = false;
	bIgnoreAllDamage = false;
	bCanTeleport = false;
	bBlocksIncomingDamage = false;
	bPersistAsLastAttacker = false;
	bAlwaysHold = false;

	//Damage Vars
	DamageLast = 0.f;
	DamageHitboxIndex = 999;
	DamageLastType = DT_None;
	LastAttacker = nullptr;
	LastAttackActor = nullptr;
	HitLoc = FVector(0.f, 0.f, 0.f);
	HitNorm = FVector(0.f, 0.f, 0.f);
	bIsBeingDestroyed = false;
}

// Called when the game starts or when spawned
void AAttackBase::BeginPlay()
{
	Super::BeginPlay();
	SetHitboxOwner();
	IncrementOwnerStats();
}

// Called every frame
void AAttackBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AAttackBase::SetHitboxOwner()
{
	if (!OwningCharacter)
		OwningCharacter = Cast<ABaseCharacter>(UncastedOwner);
	if (OwningCharacter)
	{
		//UE_LOG(LogTemp, Warning, TEXT("owning character is good"));
		AttackMultiplier = OwningCharacter->GetAttackMultiplier(bIsMelee);
		Team = OwningCharacter->Team;
		for (UHitbox* Hitbox : Hitboxes)
		{
			if (Hitbox)
			{
				Hitbox->OwningCharacter = OwningCharacter;
				//UE_LOG(LogTemp, Warning, TEXT("Set Owner, hitbox was valid"));
				Hitbox->OwningActor = this;
				Hitbox->FinalDamage = Hitbox->BaseDamage * AttackMultiplier;
				Hitbox->FriendlyFireMultiplier *= OwningCharacter->FriendlyFireMultiplier;
			}
			else
			{
				//UE_LOG(LogTemp, Warning, TEXT("hit box not valid"));
			}
		}
	}
	/*else
	{
		UE_LOG(LogTemp, Warning, TEXT("owning character is no good >:("));
	}*/
}

void AAttackBase::HitboxDamage()
{
	TArray<AActor*> OverlappedActors;
	for (UHitbox* Hitbox : Hitboxes)
	{
		if (Hitbox)
		{
			Hitbox->BlockedActors.Empty();
			Hitbox->GetOverlappingActors(OverlappedActors, TSubclassOf<AActor>());
			for (AActor* Actor : OverlappedActors)
			{
				Hitbox->ProcessOverlap(Actor, nullptr, Actor->GetActorLocation(), FVector(0.f,0.f,1.f));
			}
		}
	}
}


//Damage Functions
void AAttackBase::OnAttackActorDamage(float Damage, int32 HitboxIndex, EDamageType Type, ABaseCharacter * DamageCauser, AAttackBase* AttackActor, FVector HitLocation, FVector HitNormal)
{
	//Make sure the damage type isn't "none" & the team isn't the same & we aren't ignoreing all damage & we aren't attacking ourselves
	if (Type != DT_None && (Team != AttackActor->Team || Team == CT_NoTeam) && !bIgnoreAllDamage && AttackActor != this)
	{
		UE_LOG(LogTemp, Warning, TEXT("Ouch, attack took damage"));
		//Multiply Damage by DamageTypeMultiplier
		if (WeaknessMap.Contains(Type))
		{
			Damage = Damage * WeaknessMap.FindRef(Type);
		}
		//if hitbox is higher priority, override
		if (DamageHitboxIndex > HitboxIndex)
		{
			DamageLast = Damage;
			DamageHitboxIndex = HitboxIndex;
			DamageLastType = Type;
			LastAttacker = DamageCauser;
			LastAttackActor = AttackActor;
			HitLoc = HitLocation;
			HitNorm = HitNormal;
		}
		//if a priority tie, accept higher damage
		else if (DamageHitboxIndex == HitboxIndex && Damage > DamageLast)
		{
			DamageLast = Damage;
			DamageHitboxIndex = HitboxIndex;
			DamageLastType = Type;
			LastAttacker = DamageCauser;
			LastAttackActor = AttackActor;
			HitLoc = HitLocation;
			HitNorm = HitNormal;
		}
		//set timer for infliction
		if (Damage != 0.f && !bIsBeingDestroyed)
		{
			//	GetWorldTimerManager().SetTimer(DamageTimer, this, &AAttackBase::OnAttackActorDamageInflict, 0.01f, false, 0.01f);
			DamageTimerDelegate.BindUFunction(this, FName("OnAttackActorDamageInflict"));
			if (bAlwaysHold)
			{
				GetWorldTimerManager().SetTimer(DamageTimer, this, &AAttackBase::OnAttackActorDamageInflict, 0.1f, false, 0.1f);
			}
			else
			{
				GetWorldTimerManager().SetTimerForNextTick(this, &AAttackBase::OnAttackActorDamageInflict);
			}
			UE_LOG(LogTemp, Warning, TEXT("Set the delagate"));
		}
	}
}

void AAttackBase::OnAttackActorDamageInflict()
{
	//add attacker to array
	if (LastAttacker != nullptr)
	{
		//DamagedBy.AddUnique(LastAttacker);

	//Tell the attack actor that they were successful
		if (LastAttackActor != nullptr)
		{
			LastAttackActor->OnAttackSuccess(DamageLast, this, HitLoc, HitNorm);
		}
		//Call BP function
		PostDamage(DamageLast, DamageLastType, LastAttacker, LastAttackActor, HitLoc, HitNorm);
	}
	//Subtract health
	float damageDelt = DecreaseAttackHealth(DamageLast);
	
	//Inflict damage on the hitbox if applicible
	if (bBlocksIncomingDamage && LastAttackActor != nullptr)
	{
		//Is this actor dead? Do we care?
		if (Health >= 0.f || bAlwaysHold)
		{
			LastAttackActor->OnAttackActorDamage(damageDelt, 0, DT_Normal, OwningCharacter, this, LastAttackActor->GetActorLocation(), FVector(0.f, 0.f, 1.f));
		}
	}
	//Reset varibles
	DamageHitboxIndex = 999;
	DamageLast = 0.f;
}

void AAttackBase::PostDamage_Implementation(float Damage, EDamageType DamageType, ABaseCharacter * DamageCauser, AAttackBase* AttackActor, FVector HitLocation, FVector HitNormal)
{
}

float AAttackBase::DecreaseAttackHealth(float damage)
{
	UE_LOG(LogTemp, Warning, TEXT("Took Damage"));
	Health -= damage;
	if (Health <= 0.f)
	{
		bIsBeingDestroyed = true;
		GetWorldTimerManager().SetTimer(DestroyTimer, this, &AAttackBase::DestroyAttack, 0.1f, false, 0.1f);
		bIgnoreAllDamage = true;
		return Health + damage;
	}
	else
	{
		return damage;
	}
}

void AAttackBase::DestroyAttack()
{
	if (DamageTimer.IsValid())
		GetWorldTimerManager().ClearTimer(DamageTimer);
	DamageTimerDelegate.Unbind();
	Destroy();
}

void AAttackBase::IncrementOwnerStats()
{
	if (OwningCharacter == NULL)
	{
		OwningCharacter = Cast<ABaseCharacter>(UncastedOwner);
	}

	if (OwningCharacter) //if the cast worked
	{
		switch (AbilityType)
		{
		case AT_Offensive:
			OwningCharacter->TemporaryStats.OffensiveAbilityCounter++;
			break;
		case AT_Defensive:
			OwningCharacter->TemporaryStats.DefensiveAbilityCounter++;
			break;
		case AT_Utility:
			OwningCharacter->TemporaryStats.UtilityAbilityCounter++;
			break;
		case AT_Melee:
			OwningCharacter->TemporaryStats.MeleeCounter++;
			break;
		default:
			break;
		}

		if (AbilityType != AT_Melee)
		{
			OwningCharacter->TemporaryStats.CurrentCreativity++;
		}
	}

	if (OwningCharacter)
	{
		//GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Emerald, FString::Printf(TEXT("O (%i) D (%i) U (%i) M (%i)"), OwningCharacter->TemporaryStats.OffensiveAbilityCounter, OwningCharacter->TemporaryStats.DefensiveAbilityCounter, OwningCharacter->TemporaryStats.UtilityAbilityCounter, OwningCharacter->TemporaryStats.MeleeCounter));
	}
}

void AAttackBase::OnAttackSuccess(float Damage, AActor * HitTarget, FVector HitLocation, FVector HitNormal)
{
	PostAttackSuccess(Damage, HitTarget, HitLocation, HitNormal);
	if (bDestroyOnHit)
	{
		DamageTimerDelegate.Unbind();
		bIsBeingDestroyed = true;
		//Health = 0.f;
		DestroyAttack();
	}
}

void AAttackBase::PostAttackSuccess_Implementation(float Damage, AActor * HitTarget, FVector HitLocation, FVector HitNormal)
{
}

void AAttackBase::BeginDestroy()
{
	Super::BeginDestroy();
	if (DamageTimer.IsValid())
		GetWorldTimerManager().ClearTimer(DamageTimer);
	DamageTimerDelegate.Unbind();
}