// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "RoomBase.h"
#include "MiniArena.generated.h"

UCLASS()
class CAPSTONE_API AMiniArena : public AActor
{
	GENERATED_BODY()

		/** The activation trigger for the actor */
		UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Arena, meta = (AllowPrivateAccess = "true"))
		class UBoxComponent* ActivationTrigger;

public:	
	// Sets default values for this actor's properties
	AMiniArena();

	//The radius this arena is allowed to spawn enemies
	UPROPERTY(BlueprintReadWrite, EditAnywhere, category = "Spawning")
		float SpawningRadius = 100.f;

	//The location of the owning room's mesh in world space
	UPROPERTY(BlueprintReadWrite, EditAnywhere, category = "Spawning")
		FVector OwningRoomMeshLocation = FVector(0.f,0.f,0.f);

	//The location of the owning room's mesh in world space
	UPROPERTY(BlueprintReadWrite, EditAnywhere, category = "Spawning")
		ARoomBase* OwningRoom;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};
