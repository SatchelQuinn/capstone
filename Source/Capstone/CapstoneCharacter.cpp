// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

#include "CapstoneCharacter.h"
#include "HeadMountedDisplayFunctionLibrary.h"
#include "Camera/CameraComponent.h"
#include "Components/CapsuleComponent.h"
#include "Components/InputComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/Controller.h"
#include "GameFramework/SpringArmComponent.h"
#include "Components/MaterialBillboardComponent.h"
#include "Particles/ParticleSystemComponent.h"
#include "Kismet/GameplayStatics.h"
#include "Runtime/Engine/Classes/Kismet/KismetMathLibrary.h"

//////////////////////////////////////////////////////////////////////////
// ACapstoneCharacter

ACapstoneCharacter::ACapstoneCharacter()
{
	// Set size for collision capsule
	GetCapsuleComponent()->InitCapsuleSize(42.f, 96.0f);

	// set our turn rates for input
	BaseTurnRate = 45.f;
	BaseLookUpRate = 45.f;

	// Don't rotate when the controller rotates. Let that just affect the camera.
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	// Configure character movement
	GetCharacterMovement()->bOrientRotationToMovement = true; // Character moves in the direction of input...	
	GetCharacterMovement()->RotationRate = FRotator(0.0f, 540.0f, 0.0f); // ...at this rotation rate
	GetCharacterMovement()->JumpZVelocity = 600.f;
	GetCharacterMovement()->AirControl = 0.2f;

	// Create a camera boom (pulls in towards the player if there is a collision)
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent);
	CameraBoom->TargetArmLength = 300.0f; // The camera follows at this distance behind the character	
	CameraBoom->bUsePawnControlRotation = true; // Rotate the arm based on the controller

	// Create a follow camera
	FollowCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("FollowCamera"));
	FollowCamera->SetupAttachment(CameraBoom, USpringArmComponent::SocketName); // Attach the camera to the end of the boom and let the boom adjust to match the controller orientation
	FollowCamera->bUsePawnControlRotation = false; // Camera does not rotate relative to arm

	// Create a cursor component.
	Cursor = CreateDefaultSubobject<UMaterialBillboardComponent>(TEXT("Cursor"));
	Cursor->SetupAttachment(RootComponent);
	Cursor->SetVisibility(false);

	// Create a player marker component.
	PlayerMarkerParticle = CreateDefaultSubobject<UParticleSystemComponent>(TEXT("Player Marker"));
	PlayerMarkerParticle->SetupAttachment(RootComponent);
	PlayerMarkerParticle->SetAutoActivate(true);

	// Note: The skeletal mesh and anim blueprint references on the Mesh component (inherited from Character) 
	// are set in the derived blueprint asset named MyCharacter (to avoid direct content references in C++)


	//Change Attributes from BaseCharacter
	bCanDash = true;
	bCanLastStand = true;
	bGivesEXP = false;
	bCanLevelUp = true;
	bIsCameraTarget = bCanBeCameraTarget;
	CursorZOffset = 30.f;

}

//////////////////////////////////////////////////////////////////////////
// Input

void ACapstoneCharacter::SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent)
{
	// Set up gameplay key bindings
	check(PlayerInputComponent);
	PlayerInputComponent->BindAction("Jump", IE_Pressed, this, &ACharacter::Jump);
	PlayerInputComponent->BindAction("Jump", IE_Released, this, &ACharacter::StopJumping);

	PlayerInputComponent->BindAxis("MoveForward", this, &ACapstoneCharacter::MoveForward);
	PlayerInputComponent->BindAxis("MoveRight", this, &ACapstoneCharacter::MoveRight);

	// We have 2 versions of the rotation bindings to handle different kinds of devices differently
	// "turn" handles devices that provide an absolute delta, such as a mouse.
	// "turnrate" is for devices that we choose to treat as a rate of change, such as an analog joystick
	PlayerInputComponent->BindAxis("Turn", this, &APawn::AddControllerYawInput);
	PlayerInputComponent->BindAxis("TurnRate", this, &ACapstoneCharacter::TurnAtRate);
	PlayerInputComponent->BindAxis("LookUp", this, &APawn::AddControllerPitchInput);
	PlayerInputComponent->BindAxis("LookUpRate", this, &ACapstoneCharacter::LookUpAtRate);

	//Bind cursor movement
	PlayerInputComponent->BindAxis("CursorUp", this, &ACapstoneCharacter::CursorMoveUp);
	PlayerInputComponent->BindAxis("CursorRight", this, &ACapstoneCharacter::CursorMoveRight);

	// handle touch devices
	PlayerInputComponent->BindTouch(IE_Pressed, this, &ACapstoneCharacter::TouchStarted);
	PlayerInputComponent->BindTouch(IE_Released, this, &ACapstoneCharacter::TouchStopped);

	// VR headset functionality
	PlayerInputComponent->BindAction("ResetVR", IE_Pressed, this, &ACapstoneCharacter::OnResetVR);
}

// Called every frame
void ACapstoneCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	//update cursor location
	if (bCursorActive)
	{
		UpdateCursor(DeltaTime);
	}
	
}
void ACapstoneCharacter::BeginPlay()
{
	Super::BeginPlay();

}
void ACapstoneCharacter::OnResetVR()
{
	UHeadMountedDisplayFunctionLibrary::ResetOrientationAndPosition();
}

void ACapstoneCharacter::CursorMoveUp(float Rate)
{
	//Get axis varibles 
	float forwardaxis = FMath::Abs(InputComponent->GetAxisValue(TEXT("MoveForward")));
	float rightaxis = FMath::Abs(InputComponent->GetAxisValue(TEXT("MoveRight")));
	float cursorrightaxis = FMath::Abs(InputComponent->GetAxisValue(TEXT("CursorRight")));

	//Are we tilting our stick while not moving?
	if ((bCursorActive || FMath::Abs(Rate) > .1f) && forwardaxis < .05f && rightaxis < .05f)
	{
		if (!bCursorActive)
			ActivateCursor();
		CursorDistance2D.X += Rate * CursorSpeed * GetWorld()->GetDeltaSeconds();
	}
	else
	{
		//if the cursor is active and we aren't moving the both sticks
		if (bCursorActive && (forwardaxis > .05f || rightaxis > .05f || cursorrightaxis < .1f))
		{
			DeactivateCursor();
		}
	}
}

void ACapstoneCharacter::CursorMoveRight(float Rate)
{
	//Get axis varibles 
	float forwardaxis = FMath::Abs(InputComponent->GetAxisValue(TEXT("MoveForward")));
	float rightaxis = FMath::Abs(InputComponent->GetAxisValue(TEXT("MoveRight")));
	float cursorupaxis = FMath::Abs(InputComponent->GetAxisValue(TEXT("CursorUp")));

	//Are we tilting our stick while not moving?
	if ((bCursorActive || FMath::Abs(Rate) > .1f) && forwardaxis < .05f && rightaxis < .05f)
	{
		if (!bCursorActive)
			ActivateCursor();
		CursorDistance2D.Y += Rate * CursorSpeed * GetWorld()->GetDeltaSeconds();
	}
	else
	{
		//if the cursor is active and we aren't moving the both sticks
		if (bCursorActive && (forwardaxis > .05f || rightaxis > .05f || cursorupaxis < .1f))
		{
			DeactivateCursor();
		}
	}
}

void ACapstoneCharacter::TouchStarted(ETouchIndex::Type FingerIndex, FVector Location)
{
		Jump();
}

void ACapstoneCharacter::TouchStopped(ETouchIndex::Type FingerIndex, FVector Location)
{
		StopJumping();
}

void ACapstoneCharacter::TurnAtRate(float Rate)
{
	// calculate delta for this frame from the rate information
	AddControllerYawInput(Rate * BaseTurnRate * GetWorld()->GetDeltaSeconds());
}

void ACapstoneCharacter::LookUpAtRate(float Rate)
{
	// calculate delta for this frame from the rate information
	AddControllerPitchInput(Rate * BaseLookUpRate * GetWorld()->GetDeltaSeconds());
}

void ACapstoneCharacter::MoveForward(float Value)
{
	if ((Controller != NULL) && (Value != 0.0f))
	{
		// find out which way is forward
		const FRotator Rotation = Controller->GetControlRotation();
		const FRotator YawRotation(0, Rotation.Yaw, 0);

		// get forward vector
		//const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::X);
		const FVector Direction = FVector(1.f, 0.f, 0.f);
		AddMovementInput(Direction, Value);
	}
}

void ACapstoneCharacter::MoveRight(float Value)
{
	if ( (Controller != NULL) && (Value != 0.0f) )
	{
		// find out which way is right
		const FRotator Rotation = Controller->GetControlRotation();
		const FRotator YawRotation(0, Rotation.Yaw, 0);
	
		// get right vector 
		//const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::Y);
		// add movement in that direction
		const FVector Direction = FVector(0.0f, 1.f, 0.f);
		AddMovementInput(Direction, Value);
	}
}

void ACapstoneCharacter::OnStandardAbilityUsed()
{

}

void ACapstoneCharacter::UpdateCursor(float DeltaTime)
{
	CursorDistance2D = CursorDistance2D.GetClampedToMaxSize(CursorRange);
	CursorTargetLoc.X = CursorDistance2D.X + GetActorLocation().X;
	CursorTargetLoc.Y = CursorDistance2D.Y + GetActorLocation().Y;
	//Camera trace height
	float CameraHeight = UGameplayStatics::GetPlayerCameraManager(GetWorld(),0)->GetCameraLocation().Z;
	//trace for ground
	FHitResult Hit;
	float RayLength = FMath::Abs((CameraHeight - GetActorLocation().Z) *2);
	FVector StartLocation = FVector(CursorTargetLoc.X, CursorTargetLoc.Y, CameraHeight);
	FVector EndLocation = StartLocation + FVector(0.f, 0.f, -RayLength);
	FCollisionQueryParams CollisionParameters;
	CollisionParameters.AddIgnoredActor(this);
	GetWorld()->LineTraceSingleByChannel(Hit, StartLocation, EndLocation, ECollisionChannel::ECC_GameTraceChannel3, CollisionParameters);
	//DrawDebugLine(GetWorld(), StartLocation, EndLocation, FColor::Green, true, -1, 0, 1.f);
	
	if (Hit.bBlockingHit)
	{
		CursorTargetLoc.Z = Hit.ImpactPoint.Z + CursorZOffset;
		//UE_LOG(LogTemp, Warning, TEXT("Hit Loc: %f"), Hit.ImpactPoint.Z);
	}
	
	Cursor->SetWorldLocation(FMath::VInterpTo(FVector(Cursor->GetComponentLocation().X, Cursor->GetComponentLocation().Y, CursorTargetLoc.Z), CursorTargetLoc, DeltaTime, 10.f));
	if (bCursorFollowsRotation)
	{
		Cursor->SetWorldRotation(FMath::RInterpTo(Cursor->GetComponentRotation(), GetActorRotation(), DeltaTime, 10.f));
	}

	//Update the player's rotation when not moving
	if (GetVelocity().Size() < 10.f)
	{
		FRotator ActorRot = GetActorRotation();
		FRotator NewRot = UKismetMathLibrary::FindLookAtRotation(GetActorLocation(), Cursor->GetComponentLocation());
		FRotator InterpRot = FMath::RInterpTo(ActorRot, FRotator(ActorRot.Pitch, NewRot.Yaw, ActorRot.Roll), DeltaTime, 10.f);
		SetActorRotation(InterpRot);
	}
}

void ACapstoneCharacter::ActivateCursor()
{
	if (!bIsIncapacitated || !bIsDead)
	{
		bCursorActive = true;
		Cursor->SetWorldLocation(GetActorLocation());
		CursorTargetLoc = GetActorLocation();
		CursorDistance2D.Set(0.f, 0.f, 0.f);
		Cursor->SetVisibility(true);
	}
}

void ACapstoneCharacter::DeactivateCursor()
{
	bCursorActive = false;
	Cursor->SetVisibility(false);
}

void ACapstoneCharacter::SetCharacterCustomDepth()
{
	Super::SetCharacterCustomDepth();
	if (PlayerMarkerSystems.IsValidIndex(PlayerIndex))
	{
		PlayerMarkerParticle->SetTemplate(PlayerMarkerSystems[PlayerIndex]);
	}
}

void ACapstoneCharacter::Incapacitate()
{
	
	Super::Incapacitate();
	DeactivateCursor();
}

void ACapstoneCharacter::Death()
{

	Super::Death();
	DeactivateCursor();
}
