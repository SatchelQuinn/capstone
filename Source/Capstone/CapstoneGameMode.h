// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "CapstoneCharacter.h"
#include "CapstonePlayerController.h"
#include "CapstoneGameMode.generated.h"

UCLASS(minimalapi)
class ACapstoneGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	ACapstoneGameMode();

	//Array of player pawns
	UPROPERTY(BlueprintReadWrite, EditAnywhere, category = "Players")
	TArray<ACapstoneCharacter*> Players;

	//If true, destroys the player pawns created by create player fuction
	UPROPERTY(BlueprintReadWrite, EditAnywhere, category = "Players")
	bool bDestroyPlayers;

	//If true, destroys the player pawns created by create player fuction
	UPROPERTY(BlueprintReadWrite, EditAnywhere, category = "Players")
		bool bUseSplitscreen;
	
public: 

	//Returns the pointer of a the centermost character. Used for spawning and teleporting characters.
	UFUNCTION(BlueprintPure, category = "Gameplay")
		ACapstoneCharacter* GetCenterCharacter();

	//Returns a location that can be used for teleporting existing players or spawning new ones.
	UFUNCTION(BlueprintPure, category = "Gameplay")
		FVector GetPlayerSpawnLocation(float Offset);

	//Spawns a player character and adds it to the Players Array
	UFUNCTION(BlueprintCallable, category = "Gameplay")
		ACapstoneCharacter* SpawnPlayer(ACapstonePlayerController* Controller, float Offset, TSubclassOf<ACapstoneCharacter> PlayerClass, bool Player1Override = false);

	//Spawns a player character and adds it to the Players Array
	UFUNCTION(BlueprintCallable, category = "Gameplay")
		bool DespawnPlayer(ACapstonePlayerController* Controller, bool Player1Override = false);

	UFUNCTION(BlueprintCallable)
	virtual void BeginPlay() override;

	//Called when the game starts. Creates player pawns that allow the controllers to move.
	UFUNCTION(BlueprintCallable, category = "Gameplay")
		void CreatePlayers();

	//Called when a new player is added to the game. Either through the spawn function or the create players function 
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, category = "Gameplay")
		void OnPlayerAdded(ACapstoneCharacter* NewPlayer);
	void OnPlayerAdded_Implementation(ACapstoneCharacter* NewPlayer);

	//Called when a player is removed from the game. Either through the spawn function or the create players function 
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, category = "Gameplay")
		void OnPlayerRemoved(int32 OldIndex, FVector Location);
	void OnPlayerRemoved_Implementation(int32 OldIndex, FVector Location);

	//Returns the fastest speed of all the active players
	UFUNCTION(BlueprintCallable, category = "Gameplay")
		float ReturnFastestSpeed();

	//Adds a player to the player array at the correct index
	UFUNCTION(BlueprintCallable, category = "Gameplay")
	void HandlePlayerArray(ACapstoneCharacter* Player);

	//Returns the number of players currently playing the game
	UFUNCTION(BlueprintCallable, BlueprintPure, category = "Gameplay")
		int32 GetNumActivePlayers();

	//Sets the status of splitscreen
	UFUNCTION(BlueprintCallable, category = "Gameplay")
		void SetSplitscreenStatus(AActor* Context, bool Status);
};



