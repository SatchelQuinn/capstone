// Fill out your copyright notice in the Description page of Project Settings.

#include "MiniArena.h"
#include "Components/BoxComponent.h"

// Sets default values
AMiniArena::AMiniArena()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	ActivationTrigger = CreateDefaultSubobject<UBoxComponent>(TEXT("Activation Trigger"));
	ActivationTrigger->SetupAttachment(RootComponent);
}

// Called when the game starts or when spawned
void AMiniArena::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AMiniArena::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

