// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Hitbox.h"
#include "AttackClasses.h"
#include "BaseCharacter.h"
#include "TimerManager.h"
#include "AttackBase.generated.h"



UCLASS()
class CAPSTONE_API AAttackBase : public AAttackClasses, public IDamageInterface
{
	GENERATED_BODY()

		//class ABaseCharacter;
public:	
	// Sets default values for this actor's properties
	AAttackBase();

	//What type is this ability classified as
	UPROPERTY(BlueprintReadWrite, EditAnywhere, category = "Attributes")
		TEnumAsByte<EAbilityType> AbilityType;

	//Character that owns this actor and all hitboxes
	UPROPERTY(BlueprintReadWrite, EditAnywhere, category = "Attributes", meta = (ExposeOnSpawn = true))
		ABaseCharacter* OwningCharacter;

	//Array of hitboxes for this actor
	UPROPERTY(BlueprintReadWrite, EditAnywhere, category = "Hitbox")
	TArray<UHitbox*> Hitboxes;

	//Map of this attacks's weaknesses. Used for clashing hitboxes.
	UPROPERTY(BlueprintReadWrite, EditAnywhere, category = "Hitbox")
		TMap<TEnumAsByte<EDamageType>, float> WeaknessMap;

	//The team this attack belongs to. Used for friendly fire calculations and reflections.
	UPROPERTY(BlueprintReadWrite, EditAnywhere, category = "Attributes")
		TEnumAsByte<ECharacterTeam> Team;

	//The current Health of this attack. Will destroy when health reaches zero.
	UPROPERTY(BlueprintReadWrite, EditAnywhere, category = "Attributes")
		float Health;

	//Multiplies all hitbox damage
	UPROPERTY(BlueprintReadOnly, category = "Hitbox")
		float AttackMultiplier;

	//Array of all hit actors
	UPROPERTY(BlueprintReadOnly, category = "Hitbox")
		TArray<AActor*> HitActors;

	//Is this attack treated as a melee move?
	UPROPERTY(BlueprintReadWrite, EditAnywhere, category = "Attributes")
		bool bIsMelee;

	//Will this actor get destroyed when one of it's hitboxes collides with a damageable actor?
	UPROPERTY(BlueprintReadWrite, EditAnywhere, category = "Attributes")
		bool bDestroyOnHit;

	//This actor will be immune to all incoming damage.
	UPROPERTY(BlueprintReadWrite, EditAnywhere, category = "Attributes")
		bool bIgnoreAllDamage;

	//This actor can teleport
	UPROPERTY(BlueprintReadWrite, EditAnywhere, category = "Attributes")
		bool bCanTeleport;

	//This actor will destroy all hitboxes overlapping it at the cost of its health. Set to true for barriers.
	UPROPERTY(BlueprintReadWrite, EditAnywhere, category = "Attributes")
		bool bBlocksIncomingDamage;

	//This actor will count kills for when a character falls in a void because of this attack
	UPROPERTY(BlueprintReadWrite, EditAnywhere, category = "Attributes")
		bool bPersistAsLastAttacker;

	//This actor will always kill it's attacker despite dying
	UPROPERTY(BlueprintReadWrite, EditAnywhere, category = "Attributes", meta = (EditCondition = "bBlocksIncomingDamage"))
		bool bAlwaysHold;
	
	

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	//Timers
	FTimerHandle DamageTimer;
	FTimerHandle DestroyTimer;
	FTimerDelegate DamageTimerDelegate;
	FTimerDelegate DestroyTimerDelegate;
	//Damage varibles	
	float DamageLast;
	int32 DamageHitboxIndex;
	EDamageType DamageLastType;
	ABaseCharacter* LastAttacker;
	AAttackBase* LastAttackActor;
	FVector HitLoc;
	FVector HitNorm;
	bool bIsBeingDestroyed;


public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	//sets the owner of all the hitbox componenets
	UFUNCTION(BlueprintCallable, category = "Hitbox")
		void SetHitboxOwner();

	//Forces the hitboxes to damage all overlapping actors
	UFUNCTION(BlueprintCallable, category = "Hitbox")
		void HitboxDamage();

	//Damage Interface Function. Sets damage based on type weakness.
	UFUNCTION(BlueprintCallable, Category = "Gameplay|Damage")
		void OnAttackActorDamage(float Damage, int32 HitboxIndex, EDamageType Type, ABaseCharacter* DamageCauser, AAttackBase* AttackActor, FVector HitLocation, FVector HitNormal);

	//Called one frame after a hitbox overlaps this actor. Clear all the temporary damage varibles, subtracts health, adds the damage causer to the array and calls PostDamage
	UFUNCTION(BlueprintCallable, category = "Gameplay|Damage")
		void OnAttackActorDamageInflict();

	//Blueprint event called after damage is inflicted on this attack actor. 
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, category = "Gameplay|Damage")
	void PostDamage(float Damage, EDamageType DamageType, ABaseCharacter* DamageCauser, AAttackBase* AttackActor, FVector HitLocation, FVector HitNormal);
	void PostDamage_Implementation(float Damage, EDamageType DamageType, ABaseCharacter* DamageCauser, AAttackBase* AttackActor, FVector HitLocation, FVector HitNormal);

	//Subtract health from this attack. Destroy if dead. Returns the actual damage done.
	UFUNCTION(BlueprintCallable, category = "Gameplay|Damage")
		float DecreaseAttackHealth(float damage);

	//Call destroy one frame later.
	UFUNCTION(BlueprintCallable, category = "Gameplay|Damage")
		void DestroyAttack();

	UFUNCTION(BlueprintCallable, category = "Gameplay|Damage")
		void IncrementOwnerStats();

	//Called after the attack does damage to a valid target
	UFUNCTION(BlueprintCallable, category = "Gameplay|Damage")
		void OnAttackSuccess(float Damage, AActor* HitTarget, FVector HitLocation, FVector HitNormal);

	//Blueprint event called after damage is inflicted on this attack actor. 
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, category = "Gameplay|Damage")
		void PostAttackSuccess(float Damage, AActor* HitTarget, FVector HitLocation, FVector HitNormal);
	void PostAttackSuccess_Implementation(float Damage, AActor* HitTarget, FVector HitLocation, FVector HitNormal);

	virtual void BeginDestroy();
};

