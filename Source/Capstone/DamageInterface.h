// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"
#include "ECapstoneEnums.h"
//#include "Hitbox.h"
#include "DamageInterface.generated.h"

// This class does not need to be modified.
UINTERFACE(Blueprintable)
class UDamageInterface : public UInterface
{
	GENERATED_BODY()
};

/**
 * 
 */
class CAPSTONE_API IDamageInterface
{
	GENERATED_BODY()

	// Add interface functions to this class. This is the class that will be inherited to implement this interface.
public:
	//Damage Interface Function
	UFUNCTION(BlueprintCallable, BlueprintImplementableEvent, Category = "Gameplay|Damage")
	void OnDamage(float Damage, int32 HitboxIndex, EDamageType Type, ABaseCharacter* DamageCauser, AAttackBase* AttackActor, FVector HitLocation, FVector HitNormal);
	
	
	//virtual void OnDamage_Implementation(float BaseDamage, int32 HitboxIndex);

	//float BaseDamage, TEnumAsByte<EDamageType> Type, int32 HitboxIndex, UHitbox* Hitbox
};
