// Fill out your copyright notice in the Description page of Project Settings.

#include "RoomBase.h"
#include "Components/StaticMeshComponent.h"
#include "GeneratorBounds.h"

// Sets default values
ARoomBase::ARoomBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	SceneComponent = CreateDefaultSubobject<USceneComponent>(TEXT("Scene"));
	SceneComponent->SetupAttachment(RootComponent);
	
	RoomMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Room Mesh"));
	RoomMesh->SetupAttachment(SceneComponent);

	GeneratorBounds = CreateDefaultSubobject<UGeneratorBounds>(TEXT("GeneratorBounds"));
	GeneratorBounds->SetupAttachment(SceneComponent);
	
}

// Called when the game starts or when spawned
void ARoomBase::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ARoomBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

