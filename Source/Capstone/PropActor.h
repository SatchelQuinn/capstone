// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/StaticMeshActor.h"
#include "GameFramework/Actor.h"
#include "PropActor.generated.h"

UCLASS()
class CAPSTONE_API APropActor : public AStaticMeshActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	APropActor();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	void RotateProp();
	void ScaleProp();

	/**
	 * @brief Scales the prop in all axises from a random number in this range. For example, 1.0 and 2.0 can spawn the prop anywhere from constant size to double its size.
	 */
	UPROPERTY(BlueprintReadWrite, EditAnywhere, category = "Prop")
		FVector2D scaleFactor;

	/**
	 * @brief The amount the prop will rotate when it spawns. Influenced by Rotation Divisor; more information in that variable.
	 */
	UPROPERTY(BlueprintReadWrite, EditAnywhere, category = "Prop")
		float rotationAmount;

	/**
	 * @brief Pass in a 0 to use the Rotation Amount to spawn the prop with a random rotation from 0 degrees to the Rotation Amount degrees. Pass in a non-negative number to divide up the rotation amount into steps. For example, a Rotation Amount of 90 and a Rotation Divsor of 4 can spawn the prop with rotation 0, 90, 180, or 270 degrees.
	 */
	UPROPERTY(BlueprintReadWrite, EditAnywhere, category = "Prop")
		int rotationDivisor;

};
