// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "TemporaryStats.h"
#include "PlayerData.generated.h"

/**
 * 
 
class CAPSTONE_API PlayerData
{
public:
	PlayerData();
	~PlayerData();
};
*/

USTRUCT(BlueprintType)
struct FPlayerData
{
	GENERATED_BODY()

		FPlayerData()
	{

	}
		UPROPERTY(BlueprintReadWrite, EditAnywhere)
			int32 Level = 1;

		UPROPERTY(BlueprintReadWrite, EditAnywhere)
			float EXP = 0;

		UPROPERTY(BlueprintReadWrite, EditAnywhere)
			TArray<FName> CurrentAbilities = { FName("MagicMissile"), FName("Healpool"),FName("Shield") };

		UPROPERTY(BlueprintReadWrite, EditAnywhere)
			int32 Precision = 0;

		UPROPERTY(BlueprintReadWrite, EditAnywhere)
			int32 Aggression = 0;

		UPROPERTY(BlueprintReadWrite, EditAnywhere)
			int32 Resilience = 0;

		UPROPERTY(BlueprintReadWrite, EditAnywhere)
			int32 Creativity = 0;

		UPROPERTY(BlueprintReadWrite, EditAnywhere)
			int32 Resourcefulness = 0;

		UPROPERTY(BlueprintReadWrite, EditAnywhere)
			int32 Kindness = 0;

		UPROPERTY(BlueprintReadWrite, EditAnywhere)
			FTemporaryStats TempStats;

		UPROPERTY(BlueprintReadWrite, EditAnywhere)
			FPersistentStats PersistStats;

		UPROPERTY(BlueprintReadWrite, EditAnywhere)
			int32 CharacterModelIndex = 0;

		UPROPERTY(BlueprintReadWrite, EditAnywhere)
			int32 CharacterMaterialIndex = 0;

		UPROPERTY(BlueprintReadWrite, EditAnywhere)
			bool bIsActive = false;

};