#pragma once

#include "CoreMinimal.h"
#include "TemporaryStats.generated.h"

USTRUCT(BlueprintType)
struct FTemporaryStats
{
	GENERATED_BODY()

	FTemporaryStats()
	{

	}

	//How many times the cursor has been activated by this character. Increases Precision. 
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
		int32 CursorActivatedCount = 0;
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
		float CursorActivatedCountWeight = 0.5f;

	//How many seconds the cursor has been activated by this character. Increases precision. Decreases Aggressiveness.
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
		float CursorActivatedTime = 0.f;
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
		float CursorActivatedTimeWeight = 0.5f;

	//How many times the Offensive ability was used.
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
		int32 OffensiveAbilityCounter = 0;
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
		float OffensiveAbilityCounterWeight = 0.7f;

	//How many times the Defensive ability was used.
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
		int32 DefensiveAbilityCounter = 0;
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
		float DefensiveAbilityCounterWeight = 1.0f;

	//How many times the Utility ability was used.
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
		int32 UtilityAbilityCounter = 0;
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
		float UtilityAbilityCounterWeight = 0.8f;

	//How many times the melee attack was used
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
		int32 MeleeCounter = 0;
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
		float MeleeCounterWeight = 0.4f;

	//How many times the melee attacks hit. Increases aggressiveness 
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
		int32 MeleeHitCounter = 0;
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
		float MeleeHitCounterWeight = 0.4f;

	//How long players heath is below 20%. Increases aggressiveness 
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
		float LowHealthTime = 0.f;
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
		float LowHealthTimeWeight = 0.04f;

	//How much this character healed their allies. Increases Kindness.
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
		float AllyHealAmount = 0.0f;
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
		float AllyHealAmountWeight = 0.5f;

	//How much this player has healed themselves.
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
		float SelfHealAmount = 0.0f;
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
		float SelfHealAmountWeight = 0.5f;

	//Number of times this character died. Increases Resilience.
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
		int32 TrackedDeathCounter = 0;
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
		float TrackedDeathCounterWeight = 0.5f;

	//Number of times this character has revived an ally
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
		int32 AllyReviveCount = 0;
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
		float AllyReviveCountWeight = 0.5f;

	//How many times you hit the best hitbox of an ability. Increases precision.
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
		int32 SweetspotCounter = 0;
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
		float SweetspotCounterWeight = 0.5f;

	//How Many times you hit a move that isn�t a sweetspot
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
		int32 SourspotCounter = 0;
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
		float SourspotCounterWeight = 0.5f;

	//How much damage dealt to enemy barriers
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
		float DamageToBarriers = 0.0f;
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
		float DamageToBarriersWeight = 0.5f;

	//Counter of how many enemy barriers the player destroyed
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
		int32 EnemyBarriersDestroyed = 0;
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
		float EnemyBarriersDestroyedWeight = 0.5f;

	//How much damage this character blocked with barriers
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
		float DamageBlocked = 0.0f;
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
		float DamageBlockedWeight = 0.5f;

	//How much damage the player has taken in general
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
		float DamageTaken = 0.0f;
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
		float DamageTakenWeight = 0.5f;

	//How much damage the player has dealt in general
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
		float DamageDealt = 0.0f;
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
		float DamageDealtWeight = 0.5f;

	//How many times a player took damage from fire
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
		int32 FireAttacksTaken = 0;
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
		float FireAttacksTakenWeight = 0.5f;

	//Amount of fire damage player inflicted
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
		int32 FireDamageCount = 0;
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
		float FireDamageCountWeight = 0.5f;

	//How many times a player took damage from ice
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
		int32 IceAttacksTaken = 0;
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
		float IceAttacksTakenWeight = 0.5f;

	//Amount of ice damage player inflicted 
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
		int32 IceDamageCount = 0;
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
		float IceDamageCountWeight = 0.5f;

	//How many times a player took damage from electricity
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
		int32 ElectricAttacksTaken = 0;
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
		float ElectricAttacksTakenWeight = 0.5f;

	//Amount of electric damage player inflicted 
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
		int32 ElectricDamageCount = 0;
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
		float ElectricDamageCountWeight = 0.5f;

	//current amount of stats gained
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
		int CurrentPrecision = 0;
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
		int CurrentAggression = 0;
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
		int CurrentResilience = 0;
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
		int CurrentCreativity = 0;
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
		int CurrentResourcefulness = 0;
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
		int CurrentKindness = 0;

};

USTRUCT(BlueprintType)
struct FPersistentStats
{
	GENERATED_BODY()

	FPersistentStats()
	{

	}

	//Enhanced heal unlocked
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
		bool UnlockedEnhancedHeal = false;
};
