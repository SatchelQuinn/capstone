// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.


#include "CapstoneGameMode.h"
#include "CapstoneCharacter.h"
#include "CapstonePlayerController.h"
#include "Kismet/GameplayStatics.h"
#include "Engine/World.h"
#include "Runtime/Core/Public/GenericPlatform/GenericPlatformMath.h"
#include "Runtime/Engine/Public/EngineUtils.h"
#include "UObject/ConstructorHelpers.h"
#include "CapstoneGameInstance.h"



ACapstoneGameMode::ACapstoneGameMode()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/ThirdPersonCPP/Blueprints/ThirdPersonCharacter"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
	// set default controller class to our Blueprinted controller
	static ConstructorHelpers::FClassFinder<APlayerController> PlayerControllerBPClass(TEXT("/Game/ThirdPersonCPP/Blueprints/BP_PlayerController"));
	if (PlayerControllerBPClass.Class != NULL)
	{
		PlayerControllerClass = PlayerControllerBPClass.Class;
	}
	//Initialize variables 
	Players.Init(nullptr, 4);
	bDestroyPlayers = true;
	bUseSplitscreen = false;
}		

void ACapstoneGameMode::BeginPlay()
{
	Super::BeginPlay();
	//Sets splitscreen to the preferred setting 
	SetSplitscreenStatus(this, bUseSplitscreen);
	//Initalizes array
	ACapstoneCharacter* player1 = Cast<ACapstoneCharacter>(UGameplayStatics::GetPlayerCharacter(GetWorld(), 0));
	ACapstonePlayerController* player1Controller = Cast<ACapstonePlayerController>(UGameplayStatics::GetPlayerController(GetWorld(), 0));
	UCapstoneGameInstance* Instance = Cast<UCapstoneGameInstance>(UGameplayStatics::GetGameInstance(GetWorld()));
	if (player1 != nullptr)
	{
		Players[0] = player1;
		player1->PlayerIndex = 0;
	}
	else
	{
		for (TActorIterator<ACapstoneCharacter> ActorItr(GetWorld()); ActorItr; ++ActorItr)
		{
			// Same as with the Object Iterator, access the subclass instance with the * or -> operators.
			ACapstoneCharacter *Player = *ActorItr;
			HandlePlayerArray(Player);
		}
	}
	//If player doesn't exist, spawn one
	if (player1 == nullptr && player1Controller)
	{
		ACapstoneCharacter* NewPlayer = nullptr;
		player1Controller->SpawnPlayerPawn();
		if (Instance)
		{
			Instance->PlayersData[0].bIsActive = true;
		}
	}
	
	//Spawn Active Players
	if (Instance)
	{
		int32 i = 1;
		for (FPlayerData pd : Instance->PlayersData)
		{
			ACapstoneCharacter* player = Cast<ACapstoneCharacter>(UGameplayStatics::GetPlayerCharacter(GetWorld(), i - 1));
			ACapstonePlayerController* playerController = Cast<ACapstonePlayerController>(UGameplayStatics::GetPlayerController(GetWorld(), i - 1));
			//If player doesn't exist, spawn one
			if (player == nullptr && playerController && pd.bIsActive)
			{
				ACapstoneCharacter* NewPlayer = nullptr;
				playerController->SpawnPlayerPawn();
				UE_LOG(LogTemp, Warning, TEXT("Spawned Player"));
			}
			UE_LOG(LogTemp, Warning, TEXT("Spawning active player loop"));
			i++;
		}
	}
	
	FTimerHandle CreatePlayersTimer;
	if (!bUseSplitscreen)
	{
		GetWorldTimerManager().SetTimer(CreatePlayersTimer, this, &ACapstoneGameMode::CreatePlayers, .1f, false, .1f);
	}
	else
	{
		//destroy all players when splitscreen is on
		for (TActorIterator<APlayerController> ActorItr(GetWorld()); ActorItr; ++ActorItr)
		{
			// Same as with the Object Iterator, access the subclass instance with the * or -> operators.
			APlayerController *Player = *ActorItr;
			if (UGameplayStatics::GetPlayerControllerID(Player) != 0)
			{
				UGameplayStatics::RemovePlayer(Player, true);
			}
		}
	}
}

void ACapstoneGameMode::CreatePlayers()
{
	APlayerController* NewPlayerController;
	ACapstoneCharacter* NewPlayerCharacter;
	FVector Location = FVector(0.0f, 0.0f, 0.0f);
	FRotator Rotation = FRotator(0.0f, 0.0f, 0.0f);
	FActorSpawnParameters SpawnInfo;
	//Loop until all players are made
	if (GetNumPlayers() == 1)
	{
		UE_LOG(LogTemp, Warning, TEXT("Player Start"));
		for (int i = 0; i < 3; i++)
		{
			UE_LOG(LogTemp, Warning, TEXT("Player Loop"));
			NewPlayerController = UGameplayStatics::CreatePlayer(this, -1, true);
			NewPlayerCharacter = Cast<ACapstoneCharacter>(NewPlayerController->GetPawn());
			//destroy the new pawn if it exists
			if (NewPlayerCharacter)
			{
				HandlePlayerArray(NewPlayerCharacter);
				if (bDestroyPlayers)
				{
					ACapstonePlayerController* NewCapstoneController = Cast<ACapstonePlayerController>(NewPlayerController);
					if (NewCapstoneController != nullptr)
					{
						DespawnPlayer(NewCapstoneController);
					}
				}
			}
		}
	}
	
}

void ACapstoneGameMode::OnPlayerAdded_Implementation(ACapstoneCharacter * NewPlayer)
{
}

void ACapstoneGameMode::OnPlayerRemoved_Implementation(int32 OldIndex, FVector Location)
{
}

float ACapstoneGameMode::ReturnFastestSpeed()
{
	float highestspeed;
	highestspeed = 0.f;
	for (ACapstoneCharacter* Character : Players)
	{
		if (Character->BaseSpeed > highestspeed)
		{
			highestspeed = Character->BaseSpeed;
		}
	}
	return highestspeed;
}

ACapstoneCharacter* ACapstoneGameMode::GetCenterCharacter()
{
	//is the player array invalid?
	if (Players[0] == nullptr)
	{
		return nullptr;
	}

	//player controller
	APlayerController* playerController = UGameplayStatics::GetPlayerController(GetWorld(), 0);
	//Viewport Size
	int32 viewportX = 0;
	int32 viewportY = 0;
	playerController->GetViewportSize(viewportX, viewportY);
	const FVector2D ViewportSize = FVector2D((float)viewportX,(float)viewportY);
	//Viewport Center            
	const FVector2D  ViewportCenter = FVector2D(ViewportSize.X / 2, ViewportSize.Y / 2);
	//max initial distance
	float distance = 90000.f;
	ACapstoneCharacter* returnPlayer = Players[0];
	
	//loop through active players
	for(int i = 0; i < Players.Num(); i++)
	{
		if (Players[i])
		{
			FVector playerLoc = Players[i]->GetActorLocation();
			FVector2D playerScreenLoc = FVector2D(0.f, 0.f);
			playerController->ProjectWorldLocationToScreen(playerLoc, playerScreenLoc);
			float playerDistance = (ViewportCenter - playerScreenLoc).Size();
			//is the current player distance shorter than the previous?
			if (playerDistance < distance)
			{
				returnPlayer = Players[i];
				distance = playerDistance;
				//UE_LOG(LogTemp, Warning, TEXT("MyCharacter's Distance is %f"), distance);
			}
		}
	}
	return returnPlayer;
}

FVector ACapstoneGameMode::GetPlayerSpawnLocation(float Offset)
{
	ACapstoneCharacter* CharRef = GetCenterCharacter();
	if (CharRef == nullptr)
	{
		//get a line trace at the center of the screen
		APlayerController* playerController = UGameplayStatics::GetPlayerController(GetWorld(), 0);
		
		FVector CameraLoc = playerController->PlayerCameraManager->GetCameraLocation();
		FVector CameraForward = playerController->PlayerCameraManager->GetActorForwardVector();
		FVector End = (CameraForward * 9000.f) + CameraLoc;
		FCollisionQueryParams CollisionParams;
		FHitResult OutHit;
		if (GetWorld()->LineTraceSingleByChannel(OutHit, CameraLoc, End, ECC_Visibility, CollisionParams))
		{
			if (OutHit.bBlockingHit)
			{
				return OutHit.Location + FVector(0.f, 0.f, FMath::Abs(Offset));
			}
		}
		//trace and player location was a failure
		else
		{
			return FVector(0.f, 0.f, 0.f);
		}
	}
	//return a location behind the center character
	FVector CharacterForward = CharRef->GetActorForwardVector();
	return CharRef->GetActorLocation() + (CharacterForward * Offset * -1.f);
}

ACapstoneCharacter* ACapstoneGameMode::SpawnPlayer(ACapstonePlayerController* Controller, float Offset, TSubclassOf<ACapstoneCharacter>  PlayerClass, bool Player1Override)
{
	UCapstoneGameInstance* Instance = Cast<UCapstoneGameInstance>(UGameplayStatics::GetGameInstance(GetWorld()));
	//Prevent Player 1 from spawning
	if (UGameplayStatics::GetPlayerController(GetWorld(), 0) == Controller && !Player1Override)
	{
		return nullptr;
	}
	//Drop out if called while active
	if (Cast<ACapstoneCharacter>(Controller->GetPawn()) != nullptr)
	{
		DespawnPlayer(Controller);
		//Remove this pawn from the active pack
		if (Instance)
		{
			int32 ID = UGameplayStatics::GetPlayerControllerID(Controller);
			Instance->PlayersData[ID].bIsActive = false;
		}
		return nullptr;
	}
	else
	{
		if (Instance)
		{
			int32 ID = UGameplayStatics::GetPlayerControllerID(Controller);
			Instance->PlayersData[ID].bIsActive = true;
		}
	}
	//Spawn new character
	FVector Location(0.0f, 0.0f, 0.0f);
	if (!Player1Override)
	{
		Location = GetPlayerSpawnLocation(Offset);
	}
	else
	{
		Location = Controller->GetPawn()->GetActorLocation();
	}
	FRotator Rotation(0.0f, 0.0f, 0.0f);
	FActorSpawnParameters SpawnInfo;
	ACapstoneCharacter* NewPlayer = GetWorld()->SpawnActor<ACapstoneCharacter>(PlayerClass, Location, Rotation, SpawnInfo);
	//Is this new pawn valid?
	if (NewPlayer != nullptr)
	{
		//Destroy any pawns we possess
		if (Controller->GetPawn() != nullptr)
			Controller->GetPawn()->Destroy();
		Controller->Possess(NewPlayer);
		HandlePlayerArray(NewPlayer);
		return NewPlayer;
	}
	//Spawn Failed. Try to force a spawn on top of the center player.
	Location = GetCenterCharacter()->GetActorLocation() + FVector(0.f, 0.f, 100.f);
	SpawnInfo.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AdjustIfPossibleButDontSpawnIfColliding;
	NewPlayer = GetWorld()->SpawnActor<ACapstoneCharacter>(PlayerClass, Location, Rotation, SpawnInfo);
	if (NewPlayer != nullptr)
	{
		if (Cast<ACapstoneCharacter>(Controller->GetPawn()) != nullptr)
			DespawnPlayer(Controller);
		else if (Controller->GetPawn() != nullptr)
			Controller->GetPawn()->Destroy();
		Controller->Possess(NewPlayer);
		HandlePlayerArray(NewPlayer);
		return NewPlayer;
	}
	return nullptr;
}

bool ACapstoneGameMode::DespawnPlayer(ACapstonePlayerController* Controller, bool Player1Override)
{
	//Prevent Player 1 from despawning
	if (UGameplayStatics::GetPlayerController(GetWorld(), 0) == Controller && !Player1Override)
	{
		return false;
	}
	
	ACapstoneCharacter* OldPlayer = Cast<ACapstoneCharacter>(Controller->GetPawn());
	if (OldPlayer != nullptr)
	{
		int32 OldIndex;
		OldIndex = OldPlayer->PlayerIndex;
		Players[OldPlayer->PlayerIndex] = nullptr;
		OnPlayerRemoved(OldIndex, OldPlayer->GetActorLocation());
	}
	bool bIsDestroyed;
	APawn* OldPawn = Controller->GetPawn();
	//Spawn new pawn
	FVector Location(0.0f, 0.0f, 0.0f);
	FRotator Rotation(0.0f, 0.0f, 0.0f);
	FActorSpawnParameters SpawnInfo;
	APawn* NewPawn = GetWorld()->SpawnActor<APawn>(APawn::StaticClass(), Location, Rotation, SpawnInfo);
	//possess new pawn and then kill the old
	Controller->Possess(NewPawn);
	bIsDestroyed = OldPawn->Destroy();
	return bIsDestroyed;
}

void ACapstoneGameMode::HandlePlayerArray(ACapstoneCharacter* Player)
{
	ACapstonePlayerController* Controller = Cast<ACapstonePlayerController>(Player->GetController());
	if (Controller)
	{
		int32 NewIndex = UGameplayStatics::GetPlayerControllerID(Controller);
		Players[NewIndex] = Player;
		Player->PlayerIndex = NewIndex;
		Player->Stencil = NewIndex + 1;
		Player->SetCharacterCustomDepth();
	}
	else
	{
		Player->PlayerIndex = Players.Add(Player);
	}
	OnPlayerAdded(Player);
}

int32 ACapstoneGameMode::GetNumActivePlayers()
{
	int32 num = 0;
	for (ACapstoneCharacter* character : Players)
	{
		if (character)
		{
			num++;
		}
	}
	return num;
}

void ACapstoneGameMode::SetSplitscreenStatus(AActor* Context, bool Status)
{
	if (Context)
	{
		GetWorld()->GetGameViewport()->SetDisableSplitscreenOverride(!Status);
	}
}
