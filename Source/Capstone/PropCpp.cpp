// Fill out your copyright notice in the Description page of Project Settings.

#include "PropCpp.h"

// Sets default values for this component's properties
UPropCpp::UPropCpp()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = false;
	// ...
}


// Called when the game starts
void UPropCpp::BeginPlay()
{
	Super::BeginPlay();

	// ...
	
}


// Called every frame
void UPropCpp::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

void UPropCpp::SpawnProp()
{
	TArray<EPropType> propTypes;
	TArray<EPropType> validTypes;
	PropTypeMap.GenerateKeyArray(propTypes);
	
	//check to see if something has been turned on
	EPropType selectedPropType = EPropType::MAX_NUM_OF_PROPS;
	int randomIndex = 0;

	for (EPropType pt : propTypes)
	{
		if (PropTypeMap[pt])
		{
			//selectedPropType = pt;
			validTypes.Add(pt);
			//break;
		}
	}

	if (validTypes.Num() <= 0)
	{
		//Error: no prop type was selected!
		return;
	}
	/*if (selectedPropType == EPropType::MAX_NUM_OF_PROPS)
	{
		//Error: no prop type was selected!
		return;
	}*/

	int rando = FMath::RandRange(0, validTypes.Num() - 1);
	selectedPropType = validTypes[rando];

	switch (selectedPropType)
	{
		case EPropType::PT_Item:
			//these are actors and are not implemented just yet...
			randomIndex = FMath::RandRange(0, Items.Num() - 1);
			SpawnActor(Items[randomIndex]);
			break;
		case EPropType::PT_Destructible:
			//these are actors and are not implemented just yet...
			break;
		case EPropType::PT_Pillar:
			//These are static props that just get instatiated into the world
			randomIndex = FMath::RandRange(0, Pillars.Num() - 1);
			SpawnActor(Pillars[randomIndex]);
			break;
		case EPropType::PT_SmallProp:
			//These are static props that just get instatiated into the world
			randomIndex = FMath::RandRange(0, SmallProps.Num() - 1);
			SpawnActor(SmallProps[randomIndex]);
			break;
		case EPropType::PT_MiniArena:
			//These are arena actors that requires a reference to the room to spawn
			randomIndex = FMath::RandRange(0, SmallProps.Num() - 1);
			SpawnMiniArena(MiniArenas[randomIndex]);
			break;
		default:
			break;
	}
}

/*void UPropCpp::SpawnProp()
{
	int maxArrayLength = Items.Num() + Pillars.Num() + Destructables.Num() + SmallProps.Num();
	int randomIndex = FMath::RandRange(0,maxArrayLength - 1);

	if (randomIndex >= Items.Num())
	{
		randomIndex -= Items.Num();
		if (randomIndex >= Pillars.Num())
		{
			randomIndex -= Pillars.Num();
			if (randomIndex >= Destructables.Num())
			{
				randomIndex -= Destructables.Num();
				if (randomIndex >= SmallProps.Num())
				{
					//error!
					UE_LOG(LogTemp, Warning, TEXT("Something went wrong with the random number..."));
				}
				else
				{
					//random index chosen was inside SmallProps array
					SpawnActor(SmallProps[randomIndex]);
				}
			}
			else
			{
				//random index chosen was inside Destructables array
				SpawnActor(Destructables[randomIndex]);
			}
		}
		else
		{
			//random index chosen was inside Pillars array
			SpawnActor(Pillars[randomIndex]);
		}
	}
	else
	{
		//random index chosen was inside Items array
		if (randomIndex < 0)
		{
			UE_LOG(LogTemp, Warning, TEXT("Something went wrong with the random number, less than 0..."));
		}
		else
		{
			SpawnActor(Items[randomIndex]);
		}
	}

	/*AActor* MyOwner = GetOwner();
	UStaticMeshComponent* pMesh = NewObject<UStaticMeshComponent>(MyOwner);
	pMesh->RegisterComponent();
	pMesh->AttachTo(this, NAME_None, EAttachLocation::SnapToTarget, false);
	pMesh->SetStaticMesh(Mesh);*/
	//pMesh->SetRelativeTransform(GetRelativeTransform());
//}*/

void UPropCpp::SpawnActor(TSubclassOf<AActor> actor)
{
	//NewPlayer = GetWorld()->SpawnActor<ACapstoneCharacter>(PlayerClass, Location, Rotation, SpawnInfo);
	if (actor)
	{
		FVector Location(0.0f, 0.0f, 0.0f);
		FRotator Rotation(0.0f, 0.0f, 0.0f);
		FActorSpawnParameters SpawnInfo;
		Location = GetComponentLocation();
		Rotation = GetComponentRotation();
		SpawnInfo.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
		AActor *newActor = GetWorld()->SpawnActor<AActor>(actor, Location, Rotation, SpawnInfo);
		//AActor * newActor = GetWorld()->SpawnActor<AActor>(actor[])
	}
}

void UPropCpp::SpawnMiniArena(TSubclassOf<AMiniArena> arena)
{
	ARoomBase* OwningRoom = Cast<ARoomBase>(GetOwner());
	//Only try if this room is valid
	if (OwningRoom)
	{
		FVector Location(0.0f, 0.0f, 0.0f);
		FRotator Rotation(0.0f, 0.0f, 0.0f);
		FActorSpawnParameters SpawnInfo;
		Location = GetComponentLocation();
		Rotation = GetComponentRotation();
		SpawnInfo.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
		AMiniArena *newArena = GetWorld()->SpawnActor<AMiniArena>(arena, Location, Rotation, SpawnInfo);
		//The new arena is valid
		if (newArena)
		{
			newArena->OwningRoom = OwningRoom;
			//Maybe send some more varbles like the room's mesh bounds or something later.
		}
	}
}

void UPropCpp::SpawnComponent(UStaticMesh *_mesh)
{
	if (_mesh)
	{
		//AActor* MyOwner = GetOwner();
		//UStaticMeshComponent* pMesh = NewObject<UStaticMeshComponent>(MyOwner);
		//pMesh->RegisterComponent();
		//pMesh->AttachTo(this, NAME_None, EAttachLocation::SnapToTarget, false);
		//pMesh->SetStaticMesh(_mesh);
		//pMesh->SetRelativeTransform(GetRelativeTransform());
	}
}

