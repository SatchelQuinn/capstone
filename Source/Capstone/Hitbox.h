// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/SphereComponent.h"
#include "ECapstoneEnums.h"
#include "BaseCharacter.h"
#include "Hitbox.generated.h"

UCLASS( ClassGroup=(Custom), Blueprintable) //,meta=(BlueprintSpawnableComponent) )
class CAPSTONE_API UHitbox : public USphereComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UHitbox();

	//Damage type of this hitbox 
	UPROPERTY(BlueprintReadWrite, EditAnywhere, category = "Stats")
		TEnumAsByte<EDamageType> DamageType;

	//Index of this hitbox. When multiple hitboxes overlap, only the lowest index hitbox will be used for damage calculation 
	UPROPERTY(BlueprintReadWrite, EditAnywhere, category = "Stats")
		int32 index;

	//The raw damage of this hitbox
	UPROPERTY(BlueprintReadWrite, EditAnywhere, category = "Stats")
		float BaseDamage;

	//The damage of this hitbox after applying multipliers
	UPROPERTY(BlueprintReadOnly, category = "Stats")
		float FinalDamage;

	//Should this hitbox be disabled on impact?
	UPROPERTY(BlueprintReadWrite, EditAnywhere, category = "Stats|Extras")
		bool bHitOnlyOnce;

	//Should this hitbox only be allowed to hit actors that it hasn't hit before?
	UPROPERTY(BlueprintReadWrite, EditAnywhere, category = "Stats|Extras")
		bool bHitUniqueActors;

	//Should this hitbox cause knockback on impact?
	UPROPERTY(BlueprintReadWrite, EditAnywhere, category = "Stats|Extras")
		bool bHasKnockback;

	//Should this hitbox cause knockback on impact?
	UPROPERTY(BlueprintReadWrite, EditAnywhere, category = "Stats|Extras", meta = (EditCondition = "bHasKnockback"))
		FVector KnockbackMultiplier;

	//Should we discard interactions with barriers?
	UPROPERTY(BlueprintReadWrite, EditAnywhere, category = "Stats|Extras")
		bool bPierceBarriers;

	//Will this hitbox damage the user?
	UPROPERTY(BlueprintReadWrite, EditAnywhere, category = "Stats|Extras")
		bool bSelfDamage;

	//Will this hitbox damage all objects, regardless of team?
	UPROPERTY(BlueprintReadWrite, EditAnywhere, category = "Stats|Extras")
		bool bFriendlyFire;
	
	//The damage type of this hitbox when it hits an ally
	UPROPERTY(BlueprintReadWrite, EditAnywhere, category = "Stats|Extras", meta = (EditCondition = "bFriendlyFire"))
		TEnumAsByte<EDamageType> FriendlyDamageType;

	//Multiplier used to affect the damage/healing amount towards friendlies
	UPROPERTY(BlueprintReadWrite, EditAnywhere, category = "Stats|Extras", meta = (EditCondition = "bFriendlyFire"))
		float FriendlyFireMultiplier;

	//Will this hitbox  test against all objects and not just other characters?
	UPROPERTY(BlueprintReadWrite, EditAnywhere, category = "Stats|Extras")
		bool bAffectsWorld;

	//Is this hitbox count for the sweetspot counter?
	UPROPERTY(BlueprintReadWrite, EditAnywhere, category = "Stats|Extras")
		bool bIsSweetspot;

	//Array of all hit actors
	UPROPERTY(BlueprintReadOnly, category = "Data")
		TArray<AActor*> HitActors;
	
	//Array of all the actors that were protected by barriers. Do not attack them. Reset for continuing attacks.
	UPROPERTY(BlueprintReadOnly, category = "Data")
		TArray<AActor*> BlockedActors;

	//The owning character of this hitbox
	UPROPERTY(BlueprintReadWrite, EditAnywhere, category = "Default")
		ABaseCharacter* OwningCharacter;

	//The owning actor of this hitbox
	UPROPERTY(BlueprintReadWrite, EditAnywhere, category = "Default")
		AAttackBase* OwningActor;

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

	//Get the owning Character of this hitbox. May not always be valid.
	UFUNCTION(BlueprintPure, Category = "Hitbox")
		ABaseCharacter* GetHitboxOwningCharacter();

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	//Bound to OnComponenntBeginOverlap
	UFUNCTION(BlueprintCallable, category = "Hitbox")
	void OnHitboxBeginOverlap(class UPrimitiveComponent* OverlappedComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

	//Bound to OnComponenntEndOverlap
	UFUNCTION(BlueprintCallable, category = "Hitbox")
	void OnHitboxEndOverlap(class UPrimitiveComponent* OverlappedComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);

	//Handles damage and other fuctions associated with overlaps
	UFUNCTION(BlueprintCallable, category = "Hitbox")
		void ProcessOverlap(class AActor* OtherActor, UPrimitiveComponent * OtherComp, FVector HitLoc, FVector HitNormal);

	//Handles knockback for objects and characters
	UFUNCTION(BlueprintCallable, category = "Hitbox")
		void ProcessKnockback(class UPrimitiveComponent* OtherComp, class AActor* OtherActor);
	
	//Traces for barriers in between this hitbox and the target
	bool TraceForBarriers(TArray<AAttackBase*> OverlappingBarriers, AActor* TargetActor);
};
