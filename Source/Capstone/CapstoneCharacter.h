// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "BaseCharacter.h"
#include "CapstoneCharacter.generated.h"

UCLASS(config=Game)
class ACapstoneCharacter : public ABaseCharacter
{
	GENERATED_BODY()

	/** Camera boom positioning the camera behind the character */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class USpringArmComponent* CameraBoom;

	/** Follow camera */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class UCameraComponent* FollowCamera;

	/** Cursor*/
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class UMaterialBillboardComponent* Cursor;

	/** Player Marker*/
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Effects, meta = (AllowPrivateAccess = "true"))
		class UParticleSystemComponent* PlayerMarkerParticle;

public:
	ACapstoneCharacter();
	
	/** Base turn rate, in deg/sec. Other scaling may affect final turn rate. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category=Camera)
	float BaseTurnRate;

	/** Base look up/down rate, in deg/sec. Other scaling may affect final rate. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category=Camera)
	float BaseLookUpRate;

	//Index of the player character in the gamemode array
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Attributes")
		int32 PlayerIndex;

	//Index of currently selected ability
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Attributes")
		int32 SelectedAbilityIndex;

	//Particle systems of the character's player marker indexed by player ID.
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (category = "Params"))
		TArray<UParticleSystem*>PlayerMarkerSystems;

	//The cursor's location in world space. Resets to the character's location on activate and deactivate
		FVector CursorTargetLoc;

	//Distance the cursor is relative to the player character
		FVector CursorDistance2D;

	//How far from the ground the cursor should be
		float CursorZOffset;

	/** Called by the controller when the special attack button is pressed*/
	UFUNCTION(BlueprintCallable, Category = "Controls")
		void OnStandardAbilityUsed();

	/** Called on Tick. Updates the cursor's position with a trace included*/
	UFUNCTION(BlueprintCallable, Category = "Controls")
		void UpdateCursor(float DeltaTime);

	/** Called when the cursor is actived. Please run the parent script*/
	UFUNCTION(BlueprintCallable, Category = "Controls")
	void ActivateCursor();
	//void ActivateCursor_Implementation();

	/** Called when the cursor is deactived. Please run the parent script*/
	UFUNCTION(BlueprintCallable, Category = "Controls")
	void DeactivateCursor();
	//void DeactivateCursor_Implementation();

	virtual void SetCharacterCustomDepth();

	virtual void Incapacitate();

	virtual void Death();
	

protected:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	/** Resets HMD orientation in VR. */
	void OnResetVR();

	/** Called for forwards/backward input */
	void MoveForward(float Value);

	/** Called for side to side input */
	void MoveRight(float Value);

	/** 
	 * Called via input to turn at a given rate. 
	 * @param Rate	This is a normalized rate, i.e. 1.0 means 100% of desired turn rate
	 */
	void TurnAtRate(float Rate);

	/**
	 * Called via input to turn look up/down at a given rate. 
	 * @param Rate	This is a normalized rate, i.e. 1.0 means 100% of desired turn rate
	 */
	void LookUpAtRate(float Rate);

	/**
	Move cursor up function
	*/
	void CursorMoveUp(float Rate);

	/**
	Move cursor right function
	*/
	void CursorMoveRight(float Rate);

	/** Handler for when a touch input begins. */
	void TouchStarted(ETouchIndex::Type FingerIndex, FVector Location);

	/** Handler for when a touch input stops. */
	void TouchStopped(ETouchIndex::Type FingerIndex, FVector Location);


protected:
	// APawn interface
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;
	// End of APawn interface

public:
	/** Returns CameraBoom subobject **/
	FORCEINLINE class USpringArmComponent* GetCameraBoom() const { return CameraBoom; }
	/** Returns FollowCamera subobject **/
	FORCEINLINE class UCameraComponent* GetFollowCamera() const { return FollowCamera; }
	/** Returns Cursor subobject **/
	FORCEINLINE class UMaterialBillboardComponent* GetCursor() const { return Cursor; }

};

