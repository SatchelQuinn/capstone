// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/SphereComponent.h"
#include "AISpawner.generated.h"


UCLASS(ClassGroup = (Custom), Blueprintable, meta=(BlueprintSpawnableComponent) )
class CAPSTONE_API UAISpawner : public USphereComponent
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	UAISpawner();

	//Map of all the AI spawns and their quantities
	UPROPERTY(BlueprintReadWrite, EditAnywhere, category = "AI")
		//TArray<TSubclassOf<ABaseCharacter>> AIToSpawn;
		TMap< TSubclassOf<ABaseCharacter >, int32> AIMap;

	//Reference to all the spawned AI 
	UPROPERTY(BlueprintReadOnly, category = "AI")
		TArray<ABaseCharacter*> SpawnedAI;

	//The emitter to spawn on AI spawn
	UPROPERTY(BlueprintReadWrite, EditAnywhere, category = "AI")
		UParticleSystem* SpawnParticle;
	
	//Should we spawn AI gradually on a timer?
	UPROPERTY(BlueprintReadWrite, EditAnywhere, category = "AI")
		bool bSpawnIncrementally;

	//Increment of spawning on the spawn timer
	UPROPERTY(BlueprintReadWrite, EditAnywhere, category = "AI", meta = (EditCondition = "bSpawnIncrementally"))
		float SpawnIncerement;

	//Is the activation volume prohibited from spawning the AI?
	UPROPERTY(BlueprintReadWrite, EditAnywhere, category = "AI")
		bool bDisableActivationVolumeSpawning;

	//The offset to raise the spawning location
	UPROPERTY(BlueprintReadWrite, EditAnywhere, category = "AI")
		float SpawnZOffset;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	FTimerHandle LingeringCheckTimer;
	FTimerHandle SpawnTickTimer;

public:	
	//Attemps to spawn AI in a random radius
	UFUNCTION(BlueprintCallable, category = "Gameplay|AI")
		ABaseCharacter* SpawnAI(TSubclassOf<ABaseCharacter> AIClass);

	//Calls Spawn AI on a timer 
	UFUNCTION(BlueprintCallable, category = "Gameplay|AI")
		void SpawnAITick();

	//Calls Spawn AI on a timer 
	UFUNCTION(BlueprintCallable, category = "Gameplay|AI")
		void ActivateSpawner();

	//Destroys all AI's that are not on screen and stops spawning
	UFUNCTION(BlueprintCallable, category = "Gameplay|AI")
		void DeactivateSpawner();

	//Destroys all AI's that are off-screen after the spawner is deactivated
	UFUNCTION(BlueprintCallable, category = "Gameplay|AI")
		void CheckLingeringAI();

};
