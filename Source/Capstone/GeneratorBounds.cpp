// Fill out your copyright notice in the Description page of Project Settings.

#include "GeneratorBounds.h"

// Sets default values for this component's properties
UGeneratorBounds::UGeneratorBounds()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = false;
	// ...
	SetCanEverAffectNavigation(false);
}


// Called when the game starts
void UGeneratorBounds::BeginPlay()
{
	Super::BeginPlay();

	// ...
	
}


// Called every frame
void UGeneratorBounds::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

