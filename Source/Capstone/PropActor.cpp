// Fill out your copyright notice in the Description page of Project Settings.

#include "PropActor.h"

// Sets default values
APropActor::APropActor()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;

	scaleFactor = FVector2D(1.0f, 1.0f);
}

// Called when the game starts or when spawned
void APropActor::BeginPlay()
{
	Super::BeginPlay();

	SetMobility(EComponentMobility::Movable);
	RotateProp();
	if (!(scaleFactor.X == 1.0f && scaleFactor.Y == 1.0f))
	{
		ScaleProp();
	}
}

// Called every frame
void APropActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void APropActor::RotateProp()
{
	float newRotation = 0.0f;
	FRotator rotator = GetActorRotation();

	if (rotationDivisor > 0)
	{
		rotator.Yaw += (rotationAmount * (int)FMath::FRandRange(0, rotationDivisor));
	}
	else
	{
		rotator.Yaw = FMath::FRandRange(0, rotationAmount);
	}

	SetActorRotation(rotator);
}

void APropActor::ScaleProp()
{
	SetActorScale3D(GetActorScale3D() * FMath::FRandRange(scaleFactor.X, scaleFactor.Y));
}

