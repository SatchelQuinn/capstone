// Fill out your copyright notice in the Description page of Project Settings.

#include "BaseCharacter.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <map> 
#include "Components/SkeletalMeshComponent.h"
#include "Components/CapsuleComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "ParticleDefinitions.h"
#include "AttackBase.h"
#include "Particles/ParticleSystem.h"
#include "Particles/ParticleSystemComponent.h"
#include "Engine/World.h"


// Sets default values
ABaseCharacter::ABaseCharacter()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	//Initalize the status particle system
	StatusParticle = CreateDefaultSubobject<UParticleSystemComponent>(TEXT("Status Particle"));
	StatusParticle->SetupAttachment(GetMesh());
	StatusParticle->SetAutoActivate(false);

	//Initalize 

	//Main Stats
	Team = CT_Other;
	Level = 1;
	EXP = 0;
	EXPNextLevel = 100;
	Health = 100.f;
	MaxHealth = Health;
	Magic = 80.f;
	MaxMagic = Magic;
	MagicRegenRate = 4.f;
	MagicRegenMultiplier = 5.f;
	BaseMeleeAttack = 100.f;
	MeleeAttackMultiplier = 1.f;
	BaseMagicAttack = 100.f;
	MagicAttackMultiplier = 1.f;
	FriendlyFireMultiplier = 1.0f;
	BaseSpeed = 30.f;
	DashSpeed = 100.f;
	MaxSpeed = 200.f;
	bCursorActive = false;
	bCursorFollowsRotation = false;
	CursorSpeed = 800.f;
	CursorRange = 500.f;
	bIsFocusing = false;
	bCanDash = false;
	DashDelay = 2.f;
	
	//Attributes
	bIsDashing = false;
	bIsDead = false;
	bIsIncapacitated = false;
	bIsChanneling = false;
	bIsGrabbed = false;
	bIsAboveGround = false;
	Stencil = 5;
	Size = CS_Medium;
	Vulnerability = DV_Normal;
	bIsMinion = false;
	bCanBeCameraTarget = false;
	bIsCameraTarget = false;
	bCanLevelUp = false;
	bCanLastStand = false;
	LastStandTime = false;
	bIsLastStanding = false;
	bGivesEXP = true;
	bCanRevive = false;
	bCanReviveAlly = false;
	bVoidOutCooldown = false;
	BaseEXPYield = 10.f;
	ReviveAmount = 0.f;
	ReviveSpeed = .33f;
	VoidOutPercent = .33f;
	
	//Personailty Stats
	Precision = 0;
	Aggression = 0;
	Resilience = 0;
	Creativity = 0;
	Resourcefulness = 0;
	Kindness = 0;

	WeaknessMap.Add(DT_Heal, -1.f);
	LastSafeTransform.Add(GetTransform());

	UMaterialInterface * Material = GetMesh()->GetMaterial(0);
	if(Material)
		DyMat = GetMesh()->CreateDynamicMaterialInstance(0, Material);

}

// Called when the game starts or when spawned
void ABaseCharacter::BeginPlay()
{
	Super::BeginPlay();
	UpdateSpeed();
	

	//Set timer for characters that can dash
	if (bCanDash)
	{
		GetWorldTimerManager().SetTimer(FDashTimer, this, &ABaseCharacter::CheckDash, .25f, true, 1.f);
	}
	//Set the timer that counts the safe locations
	if (VoidOutPercent < 1.f)
	{
		GetWorldTimerManager().SetTimer(FSafeLocationTimer, this, &ABaseCharacter::SetSafeLocation, .9f, true, 1.f);
	}
	SetCharacterCustomDepth();
	LookUpAbilities(AbilityNames);

	if (bCanLevelUp)
	{
		MainStatTypes.Empty();
		MainStatTypes.Add(ST_Precision, TemporaryStats.CurrentPrecision);
		MainStatTypes.Add(ST_Aggression, TemporaryStats.CurrentAggression);
		MainStatTypes.Add(ST_Resilience, TemporaryStats.CurrentResilience);
		MainStatTypes.Add(ST_Creativity, TemporaryStats.CurrentCreativity);
		MainStatTypes.Add(ST_Resourcefulness, TemporaryStats.CurrentResourcefulness);
		MainStatTypes.Add(ST_Kindess, TemporaryStats.CurrentKindness);
		UpdatePlayerStats();
		GetWorldTimerManager().SetTimer(FSweepForNearbyCharacters, this, &ABaseCharacter::SweepForCharactersNearby, 1.0f, true, 1.0f);
		GetWorldTimerManager().SetTimer(FCursorActive, this, &ABaseCharacter::CheckIfCursorActive, 0.5f, true, 1.0f);
	}
}

void ABaseCharacter::UpdatePlayerVariablesOnTick()
{
	if (Health < MaxHealth / 80.0f && Health > 0.0f)
	{
		TemporaryStats.LowHealthTime += GetWorld()->GetDeltaSeconds();
	}
}

// Called every frame
void ABaseCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	//UpdateSpeed();
	if (bCanRevive && bIsIncapacitated)
	{
		ReviveTick(DeltaTime);
	}
	else
	{
		RegenMagic(DeltaTime);
	}

	UpdatePlayerVariablesOnTick();
}

// Called to bind functionality to input
void ABaseCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
	
}

void ABaseCharacter::SetCharacterCustomDepth()
{
	GetMesh()->Activate();
	USkeletalMeshComponent* MyMesh = GetMesh();
	MyMesh->SetRenderCustomDepth(true);
	if (bIsMinion)
	{
		switch (Team)
		{
		case CT_Player:
			Stencil = 6;
			break;
		case CT_Enemy:
			Stencil = 7;
			break;
		case CT_NoTeam:
			Stencil = 7;
			break;
		case CT_Other:
			Stencil = 7;
			break;
		default:
			Stencil = 7;
			break;
		}
	}
	MyMesh->SetCustomDepthStencilValue(Stencil);
}

void ABaseCharacter::UpdateSpeed()
{
	if (bIsDead || bIsIncapacitated || bIsChanneling || bIsGrabbed)
	{
		GetCharacterMovement()->MaxWalkSpeed = 0.f;
	}
	else if (bIsDashing)
	{
		GetCharacterMovement()->MaxWalkSpeed = FMath::GetMappedRangeValueUnclamped(FVector2D(30.f, MaxSpeed), FVector2D(200.f, 2000.f), DashSpeed);
		//UE_LOG(LogTemp, Warning, TEXT("Dashing"));
	}
	else 
		GetCharacterMovement()->MaxWalkSpeed = FMath::GetMappedRangeValueUnclamped(FVector2D(30.f, MaxSpeed), FVector2D(200.f, 2000.f), BaseSpeed) * (!bIsFocusing * .85f + .15f);

}

bool ABaseCharacter::SetFocusingState(bool NewState, UParticleSystem * ParticleEffect)
{
	//Are we changing the state? If not, do nothing.
	if (bIsFocusing != NewState)
	{
		//We are setting the focus state to be true
		if (NewState == true)
		{
			//Are we allowed to focus at this time?
			if (!bIsIncapacitated && !bIsDead && !bIsChanneling)
			{
				bIsFocusing = true;
				UpdateSpeed();
				if (ParticleEffect)
				{
					StatusParticle->SetTemplate(ParticleEffect);
					StatusParticle->Activate(true);
				}
				return true;
			}
			else
				return false;
		}
		//We are disabling the focus state
		else
		{
			bIsFocusing = false;
			UpdateSpeed();
			if (!bIsChanneling)
				StatusParticle->Deactivate();
		}
	}
	return bIsFocusing;
}

bool ABaseCharacter::SetChannelingState(bool NewState, UParticleSystem * ParticleEffect)
{
	//We are setting the channeling state to be true
	if (NewState == true)
	{
		//Are we allowed to channel at this time?
		if (!bIsIncapacitated && !bIsDead && !bIsGrabbed)
		{
			bIsChanneling = true;
			UpdateSpeed();
			//disable focus on successful channel
			if (bIsFocusing)
			{
				SetFocusingState(false);
			}
			if (ParticleEffect)
			{
				StatusParticle->SetTemplate(ParticleEffect);
				StatusParticle->Activate(true);
			}
			return true;
		}
		else
			return false;
	}
	//We are disabling the focus state
	else
	{
		bIsChanneling = false;
		UpdateSpeed();
		if (!bIsFocusing)
			StatusParticle->Deactivate();
	}
	return false;
}

bool ABaseCharacter::SetGrabState(bool NewState, USceneComponent* GrabbingComponent, FName AttachSocketName, UParticleSystem * ParticleEffect)
{
	//We are setting the grab state to be true
	if (NewState == true)
	{
		//Are we allowed to grab at this time?
		if (!bIsIncapacitated && !bIsDead && !bIsGrabbed)
		{
			bIsGrabbed = true;
			UpdateSpeed();
			GetCharacterMovement()->GravityScale = 0.f;
			//disable focus on successful grab
			if (bIsFocusing)
			{
				SetFocusingState(false);
			}
			if (bIsChanneling)
			{
				InterruptChanneling();
			}
			if (ParticleEffect)
			{
				StatusParticle->SetTemplate(ParticleEffect);
				StatusParticle->Activate(true);
			}
			if (GrabbingComponent)
			{
				AttachToComponent(GrabbingComponent, FAttachmentTransformRules::SnapToTargetIncludingScale);
			}
			return true;
		}
		else
			return false;
	}
	//We are disabling the grab state
	else
	{
		bIsGrabbed = false;
		UpdateSpeed();
		DetachFromActor(FDetachmentTransformRules::KeepWorldTransform);
		GetCharacterMovement()->GravityScale = 1.f;
		if (!bIsFocusing && !bIsChanneling)
			StatusParticle->Deactivate();
	}
	return false;
}

void ABaseCharacter::UpdatePlayerStats()
{
	EXPNextLevel = 100 * Level;
	//CursorSpeed = 600.0f + (Precision * 5.0f);
	CursorRange = 750.0f + (Precision * 5.0f) + (Creativity * 1.0f);
	BaseMeleeAttack = 100.0f + (Aggression * 5.0f) + (Resourcefulness * 2.0f);
	MaxHealth = 100.0f + (Aggression * 3.0f) + (Resilience * 7.0f) + (Kindness * 2.0f);
	BaseSpeed = 60.0f + (Aggression * 3.0f);
	MaxMagic = 80.0f + (Creativity * 7.0f) + (Resourcefulness * 2.0f) + (Kindness * 3.0f);
	ReviveSpeed = 0.33f + (Kindness * 0.03f);
	FriendlyFireMultiplier = 1.0f + (Kindness * 0.05f);
}

void ABaseCharacter::ApplyPermanentStats()
{
	float maxOfStats = 0.0f;
	float statAverage = 10.0f; //10 is the average for the stats, currently hardcoded

	maxOfStats += TemporaryStats.CurrentPrecision;
	maxOfStats += TemporaryStats.CurrentAggression;
	maxOfStats += TemporaryStats.CurrentResilience;
	maxOfStats += TemporaryStats.CurrentCreativity;
	maxOfStats += TemporaryStats.CurrentResourcefulness;
	maxOfStats += TemporaryStats.CurrentKindness;

	Precision += (int)FMath::RoundHalfToEven((float)TemporaryStats.CurrentPrecision / maxOfStats * statAverage);
	Aggression += (int)FMath::RoundHalfToEven((float)TemporaryStats.CurrentAggression / maxOfStats * statAverage);
	Resilience += (int)FMath::RoundHalfToEven((float)TemporaryStats.CurrentResilience / maxOfStats * statAverage);
	Creativity += (int)FMath::RoundHalfToEven((float)TemporaryStats.CurrentCreativity / maxOfStats * statAverage);
	Resourcefulness += (int)FMath::RoundHalfToEven((float)TemporaryStats.CurrentResourcefulness / maxOfStats * statAverage);
	Kindness += (int)FMath::RoundHalfToEven((float)TemporaryStats.CurrentKindness / maxOfStats * statAverage);
}

//gonna change this to example weights of player variables to determine 
//which weapon to gve the player when they level-up
//EDIT: Matt: Mostly works now!
void ABaseCharacter::LevelUp()
{
	TArray<EUpgradeQualifier> qualifiers;
	TArray<FAbilityUpgradeData *> abilityUpgradeData;
	FAbilityUpgradeData *chosenUpgrade = NULL;

	Level++;
	EXPNextLevel = Level * 100;
	ApplyPermanentStats();
	UpdatePlayerStats();
	
	qualifiers = DetermineValidQualifiers();
	
	abilityUpgradeData = QueryAbilityUpgradesDataTable();
	TEnumAsByte<EAbilityType> AbilityType = AT_Other;

	chosenUpgrade = DetermineNewAbility(qualifiers, abilityUpgradeData);
	if (chosenUpgrade != NULL)
	{
		int32 abilityindex;
		abilityindex = ApplyNewUpgrade(chosenUpgrade);
		switch (abilityindex)
		{
			case 0: 
				AbilityType = AT_Offensive;
				break;
			case 1:
				AbilityType = AT_Utility;
				break;
			case 2:
				AbilityType = AT_Defensive;
				break;
			default:
				break;
		}


		OnLevelUp((FName)*chosenUpgrade->NewAbilityName, AbilityType);
	
	}
	//Could not find a valid upgrade
	else
	{
		OnLevelUp((FName)"", AbilityType);
	}
	

	//after all those shenanigans, reset the player's temp stats
	TemporaryStats = OriginalStats; //might be a problem for later !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
}

TArray<EUpgradeQualifier> ABaseCharacter::DetermineValidQualifiers()
{
	TArray<EUpgradeQualifier> qualifiers;

	//Here should go the logic to determine each qualifier. 
	//Once the logic is written, add the chosen qualifier to the qualifiers TArray.
	
	// LOGIC GOES HERE

	/*
	 * Figure qualifier for highest and lowest of the main 6 stats
	 * ===========================================================
	 */
	EUpgradeQualifier highestMainStatQualifier = UQ_HighestPrecision, lowestMainStatQualifier = UQ_LowestPrecision;
	EnumStatType HighestST = ST_Precision, LowestST = ST_Precision;
	int max = 0, min = INT_MAX;

	MainStatTypes.Empty();
	MainStatTypes.Add(ST_Precision, TemporaryStats.CurrentPrecision);
	MainStatTypes.Add(ST_Aggression, TemporaryStats.CurrentAggression);
	MainStatTypes.Add(ST_Resilience, TemporaryStats.CurrentResilience);
	MainStatTypes.Add(ST_Creativity, TemporaryStats.CurrentCreativity);
	MainStatTypes.Add(ST_Resourcefulness, TemporaryStats.CurrentResourcefulness);
	MainStatTypes.Add(ST_Kindess, TemporaryStats.CurrentKindness);

	for (auto it = MainStatTypes.CreateIterator(); it; ++it)
	{
		if (it.Value() > max)
		{
			max = it.Value();
			HighestST = it.Key();
		}
		else if (it.Value() < min)
		{
			min = it.Value();
			LowestST = it.Key();
		}
	}

	switch (HighestST)
	{
		case ST_Precision:
			highestMainStatQualifier = UQ_HighestPrecision;
			break;
		case ST_Aggression:
			highestMainStatQualifier = UQ_HighestAgression;
			break;
		case ST_Resilience:
			highestMainStatQualifier = UQ_HighestResilience;
			break;
		case ST_Creativity:
			highestMainStatQualifier = UQ_HighestCreativity;
			break;
		case ST_Resourcefulness:
			highestMainStatQualifier = UQ_HighestResourcefulness;
			break;
		default:
			highestMainStatQualifier = UQ_HighestKindness;
			break;
	}
	qualifiers.Add(highestMainStatQualifier);

	switch (LowestST)
	{
		case ST_Precision:
			lowestMainStatQualifier = UQ_LowestPrecision;
			break;
		case ST_Aggression:
			lowestMainStatQualifier = UQ_LowestAgression;
			break;
		case ST_Resilience:
			lowestMainStatQualifier = UQ_LowestResilience;
			break;
		case ST_Creativity:
			lowestMainStatQualifier = UQ_LowestCreativity;
			break;
		case ST_Resourcefulness:
			lowestMainStatQualifier = UQ_LowestResourcefulness;
			break;
		default:
			lowestMainStatQualifier = UQ_LowestKindness;
			break;
	}
	qualifiers.Add(lowestMainStatQualifier);


	/*
	 * Figure out the highest and lowest abilities the player has used
	 * ===============================================================
	 */
	TMap<int, int> AbilitiesUsedCountMap;
	AbilitiesUsedCountMap.Add(0, TemporaryStats.OffensiveAbilityCounter);
	AbilitiesUsedCountMap.Add(1, TemporaryStats.DefensiveAbilityCounter);
	AbilitiesUsedCountMap.Add(2, TemporaryStats.UtilityAbilityCounter);

	max = 0;
	min = INT_MAX;
	int highestAbilityUsed = 0, lowestAbilityUsed = 0;
	for (auto it = AbilitiesUsedCountMap.CreateIterator(); it; ++it)
	{
		if (it.Value() > max)
		{
			max = it.Value();
			highestAbilityUsed = it.Key();
		}
		else if (it.Value() < min)
		{
			min = it.Value();
			lowestAbilityUsed = it.Key();
		}
	}

	switch (highestAbilityUsed)
	{
		case 0:
			qualifiers.Add(UQ_HighestOffense);
			break;
		case 1:
			qualifiers.Add(UQ_HighestDefense);
			break;
		case 2:
			qualifiers.Add(UQ_HighestUtility);
			break;
		default:
			break;
	}
	switch (lowestAbilityUsed)
	{
		case 0:
			qualifiers.Add(UQ_LowestOffense);
			break;
		case 1:
			qualifiers.Add(UQ_LowestDefense);
			break;
		case 2:
			qualifiers.Add(UQ_LowestUtility);
			break;
		default:
			break;
	}

	/*
	 * Check the different kinds of healing qualifiers
	 * ===============================================
	 */

	if (TemporaryStats.AllyHealAmount > TemporaryStats.SelfHealAmount)
	{
		qualifiers.Add(UQ_HealedAlliesMore);
	}
	else if (TemporaryStats.SelfHealAmount > TemporaryStats.AllyHealAmount)
	{
		qualifiers.Add(UQ_HealedSelfMore);
	}
	else if (TemporaryStats.SelfHealAmount == TemporaryStats.AllyHealAmount && TemporaryStats.SelfHealAmount != 0 && TemporaryStats.AllyHealAmount != 0)
	{
		qualifiers.Add(UQ_HealedAlliesMore);
		qualifiers.Add(UQ_HealedSelfMore);
	}

	/*
	 * Damage qualifiers
	 * =================
	 */

	if (TemporaryStats.LowHealthTime <= 0.0f)
	{
		qualifiers.Add(UQ_MinimumTotalDamageTaken);
		//add the other types, if new ones are created
	}

	UE_LOG(LogTemp, Warning, TEXT("Fire attack taken: (%i)"), TemporaryStats.FireAttacksTaken);
	if (TemporaryStats.FireAttacksTaken >= 5)
	{
		qualifiers.Add(UQ_MinimumFireDamageTaken);
	}
	if (TemporaryStats.IceAttacksTaken >= 5)
	{
		qualifiers.Add(UQ_MinimumIceDamageTaken);
	}
	if (TemporaryStats.ElectricAttacksTaken >= 5)
	{
		qualifiers.Add(UQ_MinimumElectricDamageTaken);
	}

	if (TemporaryStats.DamageDealt >= 150.0f)
	{
		qualifiers.Add(UQ_MinimumTotalDamageDealt);
	}

	//Afterwards, check to see if the player has any extra
	//qualifiers they achieved elsewhere and add them to the TArray
	for (EUpgradeQualifier q : permanentQualifiersAchieved)
	{
		qualifiers.Add(q);
	} 


	//debug to figure out qualifers met
	UE_LOG(LogTemp, Warning, TEXT("Met qualifiers:"));
	for (EUpgradeQualifier q : qualifiers)
	{
		UE_LOG(LogTemp, Warning, TEXT("qualifier %s"), *GETENUMSTRING("EUpgradeQualifier", q));
	}

	return qualifiers;
}


TArray<FAbilityUpgradeData *> ABaseCharacter::QueryAbilityUpgradesDataTable()
{
	TArray<FAbilityUpgradeData *> matchingUpgradeData;

	TArray<FAbilityUpgradeData *> RawUpgradeData;
	TArray<FName> RowNames = AbilityUpgradesTable->GetRowNames();
	for (auto& name : RowNames)
	{
		FAbilityUpgradeData *row = AbilityUpgradesTable->FindRow<FAbilityUpgradeData>(name, "");
		if (row)
		{
			RawUpgradeData.Add(row);
			UE_LOG(LogTemp, Warning, TEXT("Adding row, %s"), *row->ParentAbilityName);
		}
	}

	for (FAbilityData ability : CurrentAbilities)
	{
		UE_LOG(LogTemp, Warning, TEXT("QueryAbilityUpgradesDataTable(): Looking at ability %s"), *ability.Name);
		for (auto it : RawUpgradeData)
		{
			UE_LOG(LogTemp, Warning, TEXT("Iterating through...Key is %s"), *it->ParentAbilityName);
			if (it->ParentAbilityName.Contains(ability.Name))
			{
				UE_LOG(LogTemp, Warning, TEXT("I found a value, parent is %s, new ability is %s"), *it->ParentAbilityName, *it->NewAbilityName);
				matchingUpgradeData.Add((FAbilityUpgradeData *)it);
			}
		}
	}

	return matchingUpgradeData;
}

FAbilityUpgradeData* ABaseCharacter::DetermineNewAbility(TArray<EUpgradeQualifier> qualifiers, TArray<FAbilityUpgradeData *> upgradeData)
{
	float max = 0.0f;
	float tempTotal = 0.0f;
	int32 i = 0;
	TArray<TEnumAsByte<EUpgradeQualifier>> upgradeArray;
	TArray<FAbilityUpgradeData *> chosenUpgrades;
	//TMap<TEnumAsByte<EUpgradeQualifier>, float> upgradeWeightMap;

	UE_LOG(LogTemp, Warning, TEXT("Here are the qualifiers from the array I got:"));
	for (FAbilityUpgradeData *upgrade : upgradeData)
	{
		UE_LOG(LogTemp, Warning, TEXT("Looking at upgrade for %s"), *upgrade->ParentAbilityName);
		tempTotal = 0.0f;
		i = 0;
		//upgradeWeightMap = upgrade->UpgradeWeightMap;
		
		upgrade->UpgradeWeightMap.GetKeys(upgradeArray);
		
		for (TEnumAsByte<EUpgradeQualifier> q : upgradeArray)
		{
			UE_LOG(LogTemp, Warning, TEXT("Looking for qualifier %s"), *GETENUMSTRING("EUpgradeQualifier", q));
			UE_LOG(LogTemp, Warning, TEXT("Found qualifer %s, player has value %i"), *GETENUMSTRING("EUpgradeQualifier", q), qualifiers[i]);
			if (qualifiers.Contains(q))
			{
				for (auto& it : upgrade->UpgradeWeightMap)
				{
					if (it.Key == q)
					{
						UE_LOG(LogTemp, Warning, TEXT("Found value %f"), it.Value);
						tempTotal += it.Value;
					}
				}
			}
		}
		if (tempTotal > max)
		{
			UE_LOG(LogTemp, Warning, TEXT("A new max has been found, it is %f from %s"), tempTotal, *upgrade->NewAbilityName);
			max = tempTotal;
			chosenUpgrades.Empty();
			chosenUpgrades.Add(upgrade);
		}
		else if (tempTotal == max)
		{
			UE_LOG(LogTemp, Warning, TEXT("A max has been found that matches the previous max, it is %f from %s"), tempTotal, *upgrade->NewAbilityName);
			chosenUpgrades.Add(upgrade);
		}

		tempTotal = 0.0f;
		upgradeArray.Empty();
	}

	if (chosenUpgrades.Num() > 0)
	{
		i = FMath::RandHelper(chosenUpgrades.Num());
		UE_LOG(LogTemp, Warning, TEXT("The ability I chose is index %i, and it is %s"), i, *chosenUpgrades[i]->NewAbilityName);
		return chosenUpgrades[i];
	}
	UE_LOG(LogTemp, Warning, TEXT("ERROR: Player met no upgrade qualifiers!"));
	return NULL;
}

int32 ABaseCharacter::ApplyNewUpgrade(FAbilityUpgradeData *chosenUpgrade)
{
	int32 upgradeindex;
	upgradeindex = -1;
	if (chosenUpgrade)
	{
		for (int i = 0; i < AbilityNames.Num(); i++)
		{
			if (AbilityNames[i].ToString().Equals(chosenUpgrade->ParentAbilityName))
			{
				AbilityNames[i] = FName(*chosenUpgrade->NewAbilityName);
				upgradeindex = i;
				UE_LOG(LogTemp, Warning, TEXT("The Upgrade index is %i"), upgradeindex);
				break;
			}
		}
	}
	else
	{
		return upgradeindex;
	}
	LookUpAbilities(AbilityNames);
	return upgradeindex;
}

void ABaseCharacter::OnLevelUp_Implementation(FName NewAbility, EAbilityType AbilityType)
{
}

FString ABaseCharacter::GetUpgradeHint(TArray<EUpgradeQualifier> qualifiers, TArray<FAbilityUpgradeData*> upgradeData)
{
	float max = 0.0f;
	float tempTotal = 0.0f;
	int32 i = 0;
	TArray<TEnumAsByte<EUpgradeQualifier>> upgradeArray;
	TArray<FAbilityUpgradeData *> chosenUpgrades;

	//UE_LOG(LogTemp, Warning, TEXT("Here are the qualifiers from the array I got:"));
	for (FAbilityUpgradeData *upgrade : upgradeData)
	{
		//UE_LOG(LogTemp, Warning, TEXT("Looking at upgrade for %s"), *upgrade->ParentAbilityName);
		tempTotal = 0.0f;
		i = 0;

		upgrade->UpgradeWeightMap.GetKeys(upgradeArray);

		for (TEnumAsByte<EUpgradeQualifier> q : upgradeArray)
		{
			//UE_LOG(LogTemp, Warning, TEXT("Looking for qualifier %s"), *GETENUMSTRING("EUpgradeQualifier", q));
			//UE_LOG(LogTemp, Warning, TEXT("Found qualifer %s, player has value %i"), *GETENUMSTRING("EUpgradeQualifier", q), qualifiers[i]);
			if (qualifiers.Contains(q))
			{
				for (auto& it : upgrade->UpgradeWeightMap)
				{
					if (it.Key == q)
					{
						//UE_LOG(LogTemp, Warning, TEXT("Found value %f"), it.Value);
						tempTotal += it.Value;
					}
				}
			}
		}
		if (tempTotal > max)
		{
			//UE_LOG(LogTemp, Warning, TEXT("A new max has been found, it is %f from %s"), tempTotal, *upgrade->NewAbilityName);
			max = tempTotal;
			chosenUpgrades.Empty();
			chosenUpgrades.Add(upgrade);
		}
		else if (tempTotal == max)
		{
			//UE_LOG(LogTemp, Warning, TEXT("A max has been found that matches the previous max, it is %i from %s"), tempTotal, *upgrade->NewAbilityName);
			chosenUpgrades.Add(upgrade);
		}
	}

	if (chosenUpgrades.Num() > 0)
	{
		i = FMath::RandHelper(chosenUpgrades.Num());
		//UE_LOG(LogTemp, Warning, TEXT("The ability I chose is index %i, and it is %s"), i, *chosenUpgrades[i]->NewAbilityName);
		return chosenUpgrades[i]->UpgradeHint;
	}
	//UE_LOG(LogTemp, Warning, TEXT("ERROR: Player met no upgrade qualifiers!"));
	return "unimaginative";
}

void ABaseCharacter::DetermineUpgradeHint()
{
	TArray<EUpgradeQualifier> qualifiers;
	TArray<FAbilityUpgradeData *> abilityUpgradeData;
	FString Hint;

	qualifiers = DetermineValidQualifiers();
	abilityUpgradeData = QueryAbilityUpgradesDataTable();
	Hint = GetUpgradeHint(qualifiers, abilityUpgradeData);

	OnUpgradeHint(FName(*Hint));
}

void ABaseCharacter::OnUpgradeHint_Implementation(FName NewHint)
{
}

void ABaseCharacter::LookUpAbilities(TArray<FName> NewNames)
{
	if (AbilityTable)
	{
		int32 i = 0;
		CurrentAbilities.SetNum(NewNames.Num(), true);
		for (FName name : NewNames)
		{
			
			FAbilityData *Row = AbilityTable->FindRow<FAbilityData>(name, FString(""), true);
			if (Row)
			{
				CurrentAbilities[i] = *Row;
				i++;
			}
		}
	}
}

bool ABaseCharacter::CheckMagicRequirement(float MagicRequirement)
{
	if (Magic >= MagicRequirement)
	{
		UpdateMagic(-1.f * MagicRequirement);
		return true;
	}
	return false;
}

bool ABaseCharacter::CanCharacterAct()
{
	return !bIsChanneling && !bIsIncapacitated && !bIsDead;
}

void ABaseCharacter::DamageCharacter(float Damage)
{
	if (!bIsDead && !bIsIncapacitated)
	{
		Health = FMath::Clamp(Health - Damage, 0.f, MaxHealth);
		if (Damage >= 0.0f)
		{
			TemporaryStats.DamageTaken += Damage;
			TemporaryStats.CurrentResilience += Damage / 10 > 0 ? Damage / 10 : 1;
		}
		if (Health <= 0.f)
		{
			if (bCanRevive)
			{
				Incapacitate();
			}
			else
			{
				TemporaryStats.TrackedDeathCounter++;
				Death();
			}
		}
	}
}

void ABaseCharacter::Incapacitate()
{
	bIsIncapacitated = true;
	bIsCameraTarget = false;
	UpdateSpeed();
	OnIncapacitate();
}

void ABaseCharacter::OnIncapacitate_Implementation()
{

}

void ABaseCharacter::Death()
{
	bIsDead = true;
	UpdateSpeed();
	GetCapsuleComponent()->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	GetCharacterMovement()->GravityScale = 0.f;
	GiveEXP();
	bIsCameraTarget = false;
	OnDeath();
}

void ABaseCharacter::OnDeath_Implementation()
{

}

void ABaseCharacter::UpdateMagic(float DeltaMagic)
{
	Magic = FMath::Clamp(Magic + DeltaMagic, 0.f, MaxMagic);
	//TODO Add a varible that keeps track of this
}

void ABaseCharacter::StartDash_Implementation()
{
	bIsDashing = true;
	UpdateSpeed();
}

void ABaseCharacter::CheckDash()
{
	//UE_LOG(LogTemp, Warning, TEXT("Checking Dash"));
	float charspeed = GetCharacterMovement()->Velocity.Size();
	bool bisfalling = GetCharacterMovement()->IsFalling();
	if (charspeed > 10.f && !bIsDashing && !bIsFocusing)
	{
		if (bisfalling)
		{
			return;
		}
		else if (DashDelay < DashTimerCounter)
		{
			//UE_LOG(LogTemp, Warning, TEXT("Starting Dash"));
			StartDash_Implementation();
		}
		else
		{
			DashTimerCounter += .25f;
			//UE_LOG(LogTemp, Warning, TEXT("Timer counter is increased"));
		}
	}
	else if (!(charspeed > 10.f && !bisfalling && bIsDashing) || bIsFocusing)
	{
		DashTimerCounter = 0.f;
		//UE_LOG(LogTemp, Warning, TEXT("No dash"));
		if (bIsDashing)
			EndDash();
	}
}


void ABaseCharacter::EndDash_Implementation()
{
	bIsDashing = false;
	UpdateSpeed();
}

bool ABaseCharacter::GetEXP(float DeltaEXP)
{
	DeltaEXP = FMath::CeilToInt(DeltaEXP);
	EXP += DeltaEXP;
	float expfrac = (float)EXP / (float)EXPNextLevel;
	if (CanShowUpgradeHint && expfrac >= .75f && expfrac < 1.f)
	{
		DetermineUpgradeHint();
		CanShowUpgradeHint = false;
	}
	//Are we able to level up? Handle recursivly.
	if (EXP >= EXPNextLevel)
	{
		EXP -= EXPNextLevel;
		LevelUp();
		GetEXP(0.f);
		CanShowUpgradeHint = true;
		return true;
	}
	return false;
}

void ABaseCharacter::GiveEXP()
{
	for (ABaseCharacter* Character : DamagedBy)
	{
		if (Character)
		{
			if (Character->bCanLevelUp && !(Character->bIsDead) && !(Character->bIsIncapacitated))
			{
				//TODO Convert Base EXP to final EXP
				Character->GetEXP(BaseEXPYield);
			}
		}
	}
	DamagedBy.Empty();
}

bool ABaseCharacter::IsTargetAlly(ABaseCharacter* TargetCharacter, ABaseCharacter * OtherCharacter, bool &bIsSelf)
{
	if (TargetCharacter == OtherCharacter)
	{
		bIsSelf = true;
		return true;
	}
	bIsSelf = false;
	if (TargetCharacter->Team != CT_NoTeam && TargetCharacter->Team == OtherCharacter->Team)
	{
		return true;
	}
	return false;
}

bool ABaseCharacter::IsCharacterSmallEnough(ABaseCharacter* TargetCharacter, TEnumAsByte<ECharacterSize> LargestSize)
{
	switch (TargetCharacter->Size)
	{
	case CS_Small:
		return true;
		break;
	case CS_Medium:
		return LargestSize == CS_Medium || LargestSize == CS_Large || LargestSize == CS_ExtraLarge;
		break;
	case CS_Large:
		return LargestSize == CS_Large || LargestSize == CS_ExtraLarge;
		break;
	case CS_ExtraLarge:
		return LargestSize == CS_ExtraLarge;;
		break;
	default:
		return false;
		break;
	}

	return false;
}

void ABaseCharacter::OnCharacterDamage(float Damage, int32 HitboxIndex, EDamageType Type, ABaseCharacter * DamageCauser, AAttackBase* AttackActor, FVector HitLocation, FVector HitNormal)
{
	//Make sure the damage type isn't "none" and we aren't invinvcible
	if (Type != DT_None && !(Vulnerability == DV_Invulnerable || Vulnerability == DV_Invincible))
	{
		//Multiply Damage by DamageTypeMultiplier
		if (WeaknessMap.Contains(Type))
		{
			Damage = Damage * WeaknessMap.FindRef(Type);
		}
		//if hitbox is higher priority, override
		if (DamageHitboxIndex > HitboxIndex)
		{
			DamageLast = Damage;
			DamageHitboxIndex = HitboxIndex;
			DamageLastType = Type;
			LastAttacker = DamageCauser;
			LastAttackActor = AttackActor;
			HitLoc = HitLocation;
			HitNorm = HitNormal;
		}
		//if a priority tie, accept higher damage
		else if (DamageHitboxIndex == HitboxIndex && Damage > DamageLast)
		{
			DamageLast = Damage;
			DamageHitboxIndex = HitboxIndex;
			DamageLastType = Type;
			LastAttacker = DamageCauser;
			LastAttackActor = AttackActor;
			HitLoc = HitLocation;
			HitNorm = HitNormal;
		}
		//set timer for infliction
		if (Damage != 0.f)
		{
			//GetWorldTimerManager().SetTimer(DamageTimer, this, &ABaseCharacter::OnCharacterDamageInflict, 0.01f, false, 0.01f);
			FTimerDelegate DamageTimerDelegate;
			DamageTimerDelegate.BindUFunction(this, FName("OnCharacterDamageInflict"));
			GetWorld()->GetTimerManager().SetTimerForNextTick(DamageTimerDelegate);
		}

		//find out what type of damage was taken and increment correct counter
		TemporaryStats.DamageTaken += Damage;
		switch (Type)
		{
			case DT_Fire:
				TemporaryStats.FireAttacksTaken++;
				break;
			case DT_Ice:
				TemporaryStats.IceAttacksTaken++;
				break;
			case DT_Electric:
				TemporaryStats.ElectricAttacksTaken++;
				break;
			default:
				break;
		}
	}
	//Add persistant attack actors
	else if (AttackActor)
	{
		if (AttackActor->bPersistAsLastAttacker)
		{
			LastAttackActor = AttackActor;
			LastAttacker = DamageCauser;
		}
	}
}

void ABaseCharacter::OnCharacterDamageInflict()
{
	//add attacker to array
	if (LastAttacker)
	{
		DamagedBy.AddUnique(LastAttacker);
	}
	//Call BP function
	PostDamage(DamageLast, DamageLastType, LastAttacker, LastAttackActor, HitLoc, HitNorm);
	//Subtract health
	DamageCharacter(DamageLast);
	//Tell the last attacking character thatb this attack was successful
	//UE_LOG(LogTemp, Warning, TEXT("%i"), LastAttacker);
	if (LastAttacker)
	{
		LastAttacker->OnAttackSuccess(DamageLast, DamageLastType, this, LastAttackActor, HitLoc, HitNorm);
	}
	//Tell the attack actor that this attack was successful
	if (LastAttackActor)
	{
		LastAttackActor->OnAttackSuccess(DamageLast, this, HitLoc, HitNorm);
		//UE_LOG(LogTemp, Warning, TEXT("Attack Successful");
	}
	//Reset varibles
	DamageHitboxIndex = 999;
	DamageLast = 0.f;
	//probably should null the last attacker pointer too?
}

void ABaseCharacter::PostDamage_Implementation(float Damage, EDamageType DamageType, ABaseCharacter * DamageCauser, AAttackBase* AttackActor, FVector HitLocation, FVector HitNormal)
{
}

void ABaseCharacter::PostAttackInformationUpdate(AAttackBase *AttackActor)
{
	
}

void ABaseCharacter::OnAttackSuccess(float Damage, EDamageType Type, ABaseCharacter * DamagedCharacter, AAttackBase * AttackActor, FVector HitLocation, FVector HitNormal)
{
	bool KilledTarget = DamagedCharacter->bIsDead || DamagedCharacter->bIsIncapacitated;

	switch (Type)
	{
	case DT_Heal:
		if (DamagedCharacter != this) //Assume we are healing an ally
		{
			TemporaryStats.AllyHealAmount += Damage;
			TemporaryStats.CurrentKindness++;
		}
		else //We are healing ourselves
		{
			TemporaryStats.SelfHealAmount += Damage;
		}
		break;
	case DT_Fire:
		//TemporaryStats.FireAttacksTaken++;
		TemporaryStats.FireDamageCount += Damage;
		break;
	case DT_Ice:
		//TemporaryStats.IceAttacksTaken++;
		TemporaryStats.IceDamageCount += Damage;
	case DT_Electric:
		TemporaryStats.ElectricDamageCount += Damage;
	default:
		break;
	}
	TemporaryStats.DamageDealt += Damage;

	if (AttackActor->bIsMelee)
	{
		TemporaryStats.MeleeHitCounter++;
	}

	PostAttackSuccess(Damage, Type, DamagedCharacter, AttackActor, HitLocation, HitNormal, KilledTarget);

}

void ABaseCharacter::PostAttackSuccess_Implementation(float Damage, EDamageType Type, ABaseCharacter * DamagedCharacter, AAttackBase * AttackActor, FVector HitLocation, FVector HitNormal, bool KilledTarget)
{
}

void ABaseCharacter::OnHitDestructableMesh(int ResourcefulnessAmount)
{
	TemporaryStats.CurrentResourcefulness += ResourcefulnessAmount;
	UE_LOG(LogTemp, Warning, TEXT("Gained resourcefulness, now at (%i)"), TemporaryStats.CurrentResourcefulness);
}

void ABaseCharacter::ReviveTick(float DeltaSeconds)
{
	TArray<AActor*> OverlappingActors;
	ABaseCharacter* Character;
	GetCapsuleComponent()->GetOverlappingActors(OverlappingActors,TSubclassOf<ABaseCharacter>());
	for (AActor* Actor : OverlappingActors)
	{
		Character = Cast<ABaseCharacter>(Actor);
		if (Character)
		{
			if (Character->bCanReviveAlly && Character->Team == Team && Team != CT_NoTeam && Character->CanCharacterAct())
			{
				ReviveAmount += DeltaSeconds * ReviveSpeed;
			}
			if (ReviveAmount >= 1.0f)
			{
				TemporaryStats.AllyReviveCount++;
				Revive(Character);
				break;
			}
		}
	}
	
}

void ABaseCharacter::Revive_Implementation(ABaseCharacter * AllyWhoRevived)
{
	bIsDead = false;
	bIsIncapacitated = false;
	ReviveAmount = 0.f;
	Health = MaxHealth / 2.f;
	UpdateSpeed();
	bIsCameraTarget = bCanBeCameraTarget;
}

void ABaseCharacter::OnVoidOut_Implementation()
{
	//Make sure pushing abilities get credit for this kill
	if (LastAttackActor && LastAttacker)
	{
		if (LastAttackActor->bPersistAsLastAttacker)
		{
			DamagedBy.AddUnique(LastAttacker);
		}
	}
	//Damage the character as normal
	DamageCharacter(MaxHealth * VoidOutPercent);
	bVoidOutCooldown = true;
	GetWorldTimerManager().SetTimer(FVoidOutCooldownTimer, this, &ABaseCharacter::VoidOutCooldown, 1.f, false, 1.f);
	if (!bIsDead)
	{
		GetCharacterMovement()->StopMovementImmediately();
		FTransform SafeTrans = GetSafeTransform();
		SetActorTransform(SafeTrans);
		if (bIsIncapacitated)
		{
			GetMesh()->SetWorldTransform(SafeTrans, false, nullptr, ETeleportType::ResetPhysics);
			UE_LOG(LogTemp, Warning, TEXT("teleport dead body"));
		}
	}
	//We died because of this. Add creativity.
	else if (LastAttackActor)
	{
		if (LastAttackActor->bPersistAsLastAttacker)
		{
			//UE_LOG(LogTemp, Warning, TEXT("Adding creativity"));
			TemporaryStats.CurrentCreativity += 5;
		}
	}
	if (GetMesh()->IsSimulatingPhysics() && bIsIncapacitated)
	{
		GetMesh()->ResetAllBodiesSimulatePhysics();
	}
}

FTransform ABaseCharacter::GetSafeTransform()
{
	FTransform FinalTrans = FTransform();
	FHitResult VoidHit;
	FVector StartLocation;
	FVector EndLocation;
	FCollisionQueryParams CollisionParameters;
	float RayLength = 500.f;
	CollisionParameters.AddIgnoredActor(this);
	//Line trace for the floor
	for (FTransform trans : LastSafeTransform)
	{
		StartLocation = trans.GetLocation() + FVector(0.f, 0.f, RayLength / 2);
		EndLocation = trans.GetLocation() - FVector(0.f, 0.f, RayLength / 2);
		GetWorld()->LineTraceSingleByChannel(VoidHit, StartLocation, EndLocation, ECollisionChannel::ECC_GameTraceChannel3, CollisionParameters);
		if (VoidHit.bBlockingHit)
		{
			FinalTrans = trans;
			break;
		}
	}
	return FinalTrans;
}

void ABaseCharacter::InterruptChanneling()
{
	if (ChanneledAttack)
	{
		ChanneledAttack->Destroy();
	}
	SetChannelingState(false, nullptr);
}

void ABaseCharacter::SetVulnerabilityState(TEnumAsByte<EDamageVulnerability> NewState)
{
	Vulnerability = NewState;
}

float ABaseCharacter::GetAttackMultiplier(bool bIsMelee)
{
	float multiplier = 0.f;
	if (bIsMelee)
	{
		multiplier = BaseMeleeAttack / 100.f;
		multiplier = multiplier * MeleeAttackMultiplier;
		return multiplier;
	}
	multiplier = BaseMagicAttack / 100.f;
	multiplier = multiplier * MagicAttackMultiplier;
	return multiplier;
}

void ABaseCharacter::RegenMagic(float DeltaTime)
{
	UpdateMagic(MagicRegenRate * DeltaTime + MagicRegenRate * DeltaTime * MagicRegenMultiplier * bIsFocusing);
}

void ABaseCharacter::SetSafeLocation()
{
	bool isfalling = GetCharacterMovement()->IsFalling();
	if (!isfalling && !bIsIncapacitated)
	{
		//don't add this new transform if it's similar to the old one
		if (!LastSafeTransform[0].GetLocation().Equals(GetActorLocation(), 5.f))
		{
			if (LastSafeTransform.Num() > 10)
			{
				LastSafeTransform.Pop();
			}
			LastSafeTransform.Insert(GetTransform(), 0);
		}
		bIsAboveGround = true;
	}
	//Raytrace to make sure this character isn't above the void
	if (isfalling)
	{
		FHitResult VoidHit2;
		FVector StartLocation;
		FVector EndLocation;
		FCollisionQueryParams CollisionParameters;
		float RayLength = 1000.f;
		CollisionParameters.AddIgnoredActor(this);
		//Line trace for the floor
		StartLocation = GetActorLocation();
		EndLocation = GetActorLocation() - FVector(0.f, 0.f, RayLength);
		GetWorld()->LineTraceSingleByChannel(VoidHit2, StartLocation, EndLocation, ECollisionChannel::ECC_GameTraceChannel3, CollisionParameters);
		if (VoidHit2.bBlockingHit)
		{
			bIsAboveGround = true;
		}
		else
		{
			bIsAboveGround = false;
		}
	}
}

void ABaseCharacter::VoidOutCooldown()
{
	bVoidOutCooldown = false;
}

void ABaseCharacter::SweepForCharactersNearby()
{
	TArray<FHitResult> FoundObjects;
	FCollisionQueryParams qParams;
	FCollisionResponseParams rParams;
	bool FoundAlly = false, FoundEnemy = false;

	qParams.AddIgnoredActor(this);
	GetWorld()->SweepMultiByChannel(FoundObjects, GetActorLocation(), GetActorLocation(), GetActorQuat(), ECC_Pawn, FCollisionShape::MakeSphere(200.0f), qParams, rParams);
	//UE_LOG(LogTemp, Warning, TEXT("Called SweepForEnemiesNearby(), current aggression (%i), kindness (%i)"), TemporaryStats.CurrentAggression, TemporaryStats.CurrentKindness);

	for (FHitResult hit : FoundObjects)
	{
		ABaseCharacter *c = Cast<ABaseCharacter>(hit.Actor);
		if (c)
		{
			//UE_LOG(LogTemp, Warning, TEXT("Found a base character actor (%s)"), *c->GetName());
			bool isSelf;
			if (c->IsTargetAlly(c, this, isSelf))
			{
				//UE_LOG(LogTemp, Warning, TEXT("Target is ally"));
				if (!isSelf)
				{
					//UE_LOG(LogTemp, Warning, TEXT("Target is not self"));
					FoundAlly = true;
				}
			}
			else
			{
				//UE_LOG(LogTemp, Warning, TEXT("Target is enemy"));
				FoundEnemy = true;
			}
		}
	}

	if (FoundAlly)
	{
		TemporaryStats.CurrentKindness++;
	}
	if (FoundEnemy)
	{
		TemporaryStats.CurrentAggression++;
	}
}

void ABaseCharacter::CheckIfCursorActive()
{
	if (bCursorActive)
	{
		TemporaryStats.CurrentPrecision++;
	}
}
