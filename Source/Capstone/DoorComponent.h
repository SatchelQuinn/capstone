// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ArrowComponent.h"
#include "DoorComponent.generated.h"

UENUM(BlueprintType, Meta = (Bitflags))
enum ERoomType
{
	RT_Big UMETA(DisplayName = "Big"),
	RT_Small UMETA(DisplayName = "Small"),
	RT_Hallway UMETA(DisplayName = "Hallway"),
	RT_Other UMETA(DisplayName = "Other")
};

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class CAPSTONE_API UDoorComponent : public UArrowComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UDoorComponent();

	//If true, blocks the generator from using this door
	UPROPERTY(EditAnywhere, BlueprintReadWrite, category = "Door")
		bool bDisabled = false;

	//If true, this door can be used as an enterance
	UPROPERTY(EditAnywhere, BlueprintReadWrite, category = "Door")
		bool bCanBeEntrance = true;

	//The door componrnt this door is connected to
	UPROPERTY(EditAnywhere, BlueprintReadWrite, category = "Door")
		UDoorComponent* ConnectedTo;

	//Which mesh does this door want when connecting to another door? Will not spawn a mesh when two doors with connecting meshes meet.
	UPROPERTY(EditAnywhere, BlueprintReadWrite, category = "Meshes")
		UStaticMesh* ConnectingMesh;

	//The mesh that will cap off this door when no deadend can be placed
	UPROPERTY(EditAnywhere, BlueprintReadWrite, category = "Meshes")
		UStaticMesh* SlabMesh;

	//How far forward the mesh will be spawned from the door's location, relative to the rotation of the door.
	UPROPERTY(EditAnywhere, BlueprintReadWrite, category = "Meshes")
		float SlabOffset = 0.f;

	//The types of rooms we are allowed to connect to
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Meta = (Bitmask, BitmaskEnum = "ERoomType", category = "Door"))
		int32 RoomTypeFlags = 7;

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	//Is this door compatible with this room type?
	UFUNCTION(BlueprintCallable, BlueprintPure, category = "Door")
		static bool IsDoorCompatible(UDoorComponent* Door, TEnumAsByte<ERoomType> RoomType);
};
