// Fill out your copyright notice in the Description page of Project Settings.

#include "AISpawner.h"
#include "ActivationVolume.h"
#include "BaseCharacter.h"
#include "Kismet/GameplayStatics.h"
#include "ParticleDefinitions.h"
#include "Particles/ParticleSystem.h"



// Sets default values
UAISpawner::UAISpawner()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	SetCanEverAffectNavigation(false);

	SpawnParticle = nullptr;
	bSpawnIncrementally = true;
	SpawnIncerement = .1f;
	bDisableActivationVolumeSpawning = false;
	SpawnZOffset = 0.f;
}

// Called when the game starts or when spawned
void UAISpawner::BeginPlay()
{
	Super::BeginPlay();
	
}

ABaseCharacter*  UAISpawner::SpawnAI(TSubclassOf<ABaseCharacter> AIClass)
{
	FVector Location(0.0f, 0.0f, 0.0f);
	FRotator Rotation(0.0f, 0.0f, 0.0f);
	FActorSpawnParameters SpawnInfo;
	float randX = FMath::RandRange(0.f, SphereRadius);
	float randY = FMath::RandRange(0.f, SphereRadius);
	//FTransform randomtrans = GetComponentTransform();
	//FVector FinalLoc;
	//FinalLoc = randomtrans.TransformPosition(FVector(randX, randY, 0.f));
	randX += GetComponentLocation().X;
	randY += GetComponentLocation().Y;
	Location = FVector(randX, randY, GetComponentLocation().Z + SpawnZOffset);
	Rotation = GetComponentRotation();
	SpawnInfo.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AdjustIfPossibleButDontSpawnIfColliding;
	AActor *newActor = GetWorld()->SpawnActor<AActor>(AIClass, Location, Rotation, SpawnInfo);
	ABaseCharacter* newAI = Cast<ABaseCharacter>(newActor);
	//Spawn Emitter
	if (newAI && SpawnParticle)
	{
		UGameplayStatics::SpawnEmitterAtLocation(this, SpawnParticle, Location, Rotation, true);
	}
	return newAI;
}

void UAISpawner::SpawnAITick()
{
	TArray<TSubclassOf<ABaseCharacter>> AIArray; 
	AIMap.GenerateKeyArray(AIArray);
	for (TSubclassOf<ABaseCharacter> AIClass : AIArray)
	{
		if (AIMap[AIClass] > 0)
		{
			ABaseCharacter* NewAI = SpawnAI(AIClass);
			if (NewAI)
			{
				SpawnedAI.Add(NewAI);
				int32 NewValue = AIMap[AIClass] - 1;
				AIMap.Add(AIClass, NewValue);
			}
		}
	}

}

void UAISpawner::ActivateSpawner()
{
	GetOwner()->GetWorldTimerManager().ClearTimer(LingeringCheckTimer);
	TArray<int32> AIValueArray;
	AIMap.GenerateValueArray(AIValueArray);
	int32 iterations = 0;
	for (int32 val : AIValueArray)
	{
		iterations += val;
	}
	if (iterations > 0)
	{
		if (bSpawnIncrementally)
		{
			GetOwner()->GetWorldTimerManager().SetTimer(SpawnTickTimer, this, &UAISpawner::SpawnAITick, SpawnIncerement, true, SpawnIncerement);
		}
		else
		{
			for (int32 i = 0; i < iterations; i++)
			{
				SpawnAITick();
			}
		}
	}
}

void UAISpawner::DeactivateSpawner()
{
	CheckLingeringAI();
	if (SpawnedAI.Num() > 0)
	{
		GetOwner()->GetWorldTimerManager().SetTimer(LingeringCheckTimer, this, &UAISpawner::CheckLingeringAI, 1.f, true, 1.f);
	}
	if (GetOwner()->GetWorldTimerManager().IsTimerActive(SpawnTickTimer))
	{
		GetOwner()->GetWorldTimerManager().ClearTimer(SpawnTickTimer);
	}
}

void UAISpawner::CheckLingeringAI()
{
	UE_LOG(LogTemp, Warning, TEXT("TimerTick"));
	TArray<ABaseCharacter*> NewArray;
	for (ABaseCharacter* AI : SpawnedAI)
	{
		if (AI)
		{
			TArray<UPrimitiveComponent*> OverlappedComponents;
			AI->GetOverlappingComponents(OverlappedComponents);
			//
			bool bInSight = false;
			for (UPrimitiveComponent* com : OverlappedComponents)
			{
				UActivationVolume* volume = Cast<UActivationVolume>(com);
				if (volume)
				{
					bInSight = true;
					break;
				}
			}
			//AI is still valid and within view. Don't throw him away. Don't throw away corpses either
			if (AI->bIsDead || bInSight)
			{
				NewArray.Add(AI);
				UE_LOG(LogTemp, Warning, TEXT("AI Saved"));
			}
			//AI is alive, but off screen. Destroy him and reupdate the map
			else if (!AI->bIsDead && !bInSight)
			{
				AIMap.Add(AI->GetClass(), AIMap[AI->GetClass()] + 1);
				AI->Destroy();
				UE_LOG(LogTemp, Warning, TEXT("Destroying AI"));
			}
		}
	}
	SpawnedAI.Empty();
	SpawnedAI = NewArray;
	if (SpawnedAI.Num() == 0)
	{
		GetOwner()->GetWorldTimerManager().ClearTimer(LingeringCheckTimer);
	}
}