// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AttackClasses.h"
#include "Engine/DataTable.h"
#include "ECapstoneEnums.h"
#include "LevelUpStructs.generated.h"
/**
 * 

class CAPSTONE_API LevelUpStructs
{
public:
	LevelUpStructs();
	~LevelUpStructs();


};
*/

USTRUCT(BlueprintType)
struct FAbilityData : public FTableRowBase
{
	GENERATED_BODY()

	FAbilityData()
	{

	}

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
		FString Name = "None";

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
		FString DescriptionShort = "A cool Ability!";

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
		FString DescriptionLong = "This ability uses a variety of numbers to produce results. Neat!";

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
		TSubclassOf<AAttackClasses> ActorClass;

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
		float MagicCost = 0.f;

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
		FName SpawnBoneName;
	
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
		bool bAttachToBone = false;

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
		float LocationForwardOffset = 0.f;

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
		bool bSpawnAtCursorLoc = false;

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
		bool bSpawnOnlyOnce = false;

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
		bool bRequiresChanneling = false;

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
		float ChargeTime = 0.f;

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
		bool bSpawnBeforeCharge = false;

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
		UParticleSystem* ChannelingParticle = nullptr;

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
		TEnumAsByte<EAnimationType> AnimationType = AT_None;

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
		TEnumAsByte<ECursorType> CursorType = CT_Sky;

};

UENUM(BlueprintType)
enum EUpgradeQualifier
{
	/*
	 * HIGHEST STATS GAINED THIS LEVEL
	 */

	//Precision is the highest stat gained this level
	UQ_HighestPrecision UMETA(DisplayName = "Highest Precision"),

	//Aggression is the highest stat gained this level
	UQ_HighestAgression UMETA(DisplayName = "Highest Aggression"),

	//Resilience is the highest stat gained this level
	UQ_HighestResilience UMETA(DisplayName = "Highest Resilience"),

	//Creativity is the highest stat gained this level
	UQ_HighestCreativity UMETA(DisplayName = "Highest Creativity"),

	//Resourcefulness is the highest stat gained this level
	UQ_HighestResourcefulness UMETA(DisplayName = "Highest Resourcefulness"),

	//Kindness is the highest stat gained this level
	UQ_HighestKindness UMETA(DisplayName = "Highest Kindness"),

	/*
	 * LOWEST STATS GAINED THIS LEVEL
	 */

	//Precision is the lowest stat gained this level
	UQ_LowestPrecision UMETA(DisplayName = "Lowest Precision"),

	//Aggression is the lowest stat gained this level
	UQ_LowestAgression UMETA(DisplayName = "Lowest Aggression"),

	//Resilience is the lowest stat gained this level
	UQ_LowestResilience UMETA(DisplayName = "Lowest Resilience"),

	//Creativity is the lowest stat gained this level
	UQ_LowestCreativity UMETA(DisplayName = "Lowest Creativity"),

	//Resourcefulness is the lowest stat gained this level
	UQ_LowestResourcefulness UMETA(DisplayName = "Lowest Resourcefulness"),

	//Kindness is the lowest stat gained this level
	UQ_LowestKindness UMETA(DisplayName = "Lowest Kindness"),

	/*
	 * MOST USED ABILITY TYPES
	 */

	//Player used their offensive ability the most this level
	UQ_HighestOffense UMETA(DisplayName = "Highest Offense"),

	//Player used their defensive ability the most this level
	UQ_HighestDefense UMETA(DisplayName = "Highest Defense"),

	//Player used their utility ability the most this level
	UQ_HighestUtility UMETA(DisplayName = "Highest Utility"),

	/*
	 * LEAST USED ABILITY TYPES
	 */

	//Player used their offensive ability the least this level
	UQ_LowestOffense UMETA(DisplayName = "Lowest Offense"),

	//Player used their defensive ability the least this level
	UQ_LowestDefense UMETA(DisplayName = "Lowest Defense"),

	//Player used their utility ability the least this level
	UQ_LowestUtility UMETA(DisplayName = "Lowest Utility"),

	/*
	 * DAMAGE TYPES: TAKEN
	 */

	//Player took a minimum number of damage in total to meet a certain requirement
	UQ_MinimumTotalDamageTaken UMETA(DisplayName = "Minimum Total Damage Taken"),

	//Player took a minimum number of fire damage to meet a certain requirement
	UQ_MinimumFireDamageTaken UMETA(DisplayName = "Minimum Fire Damage Taken"),

	//Player took a minimum number of ice damage to meet a certain requirement
	UQ_MinimumIceDamageTaken UMETA(DisplayName = "Minimum Ice Damage Taken"),

	//Player took a minimum number of electric damage to meet a certain requirement
	UQ_MinimumElectricDamageTaken UMETA(DisplayName = "Minimum Electric Damage Taken"),

	/*
	 * DAMAGE TYPES: DEALT
	 */

	 //Player took a minimum number of damage in total to meet a certain requirement
	 UQ_MinimumTotalDamageDealt UMETA(DisplayName = "Minimum Total Damage Dealt"),

	/*
	 * HEALS
	 */

	//Player healed their allies more than themselves
	UQ_HealedAlliesMore UMETA(DisplayName = "Healed Allies More"),

	//Player healed themselves more than their allies
	UQ_HealedSelfMore UMETA(DisplayName = "Healed Self More"),

	/*
	 * IF THE PLAYER HAS A CERTAIN ABILITY
	 */

	UQ_UnlockedEnhancedHeal UMETA(DisplayName = "Unlocked Enhanced Heal")
};

USTRUCT(BlueprintType)
struct FAbilityUpgradeData : public FTableRowBase
{
	GENERATED_BODY()

	FAbilityUpgradeData()
	{

	}

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
		FString ParentAbilityName;

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
		TMap<TEnumAsByte<EUpgradeQualifier>, float> UpgradeWeightMap;

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
		FString NewAbilityName;

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
		FString UpgradeHint;
};


