// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "AttackClasses.generated.h"

UCLASS()
class CAPSTONE_API AAttackClasses : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AAttackClasses();

	//Uncasted character that owns this attack
	UPROPERTY(BlueprintReadWrite, EditAnywhere, category = "Attributes", meta = (ExposeOnSpawn = true))
		AActor* UncastedOwner;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};
